<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function transports()
    {
        return $this->hasMany(Transport::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function contacts()
    {
        return $this->hasMany(CustomerContact::class);
    }
}
