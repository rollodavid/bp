<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Demand extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function locations()
    {
        return $this->belongsToMany(Location::class, 'demand_location');
    }

    public function prices()
    {
        return $this->hasMany(CarrierPrice::class);
    }
}
