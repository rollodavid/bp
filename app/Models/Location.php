<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function demands()
    {
        return $this->belongsToMany(Demand::class, 'demand_location');
    }

    public function transports()
    {
        return $this->belongsToMany(Transport::class, 'transport_locations');
    }

    public function type()
    {
        return $this->belongsTo(LocationType::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
}
