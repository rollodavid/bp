<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $guarded = [];
    
    use HasFactory;

    public function driver()
    {
        return $this->hasOne(Driver::class);
    }

    public function carrier()
    {
        return $this->belongsTo(Carrier::class);
    }

    public function type()
    {
        return $this->belongsTo(VehicleType::class);
    }

    public function height()
    {
        return $this->belongsTo(VehicleHeight::class);
    }

    public function transports()
    {
        return $this->hasMany(Transport::class);
    }
}
