<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function types()
    {
        return $this->belongsToMany(LocationType::class, 'locations', 'address_id', 'type_id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function carrier()
    {
        return $this->belongsTo(Carrier::class);
    }

    public function show()
    {
        return $this->city . ', ' . $this->street . ' ' . $this->number . ', ' . $this->psc;
    }

    public static function addAddress($data)
    {
        $existingAddress = Address::where('city',$data['city'])
            ->where('street',$data['street'])
            ->where('number',$data['number'])
            ->where('psc',$data['psc'])->first();

        return ($existingAddress == null ? ['address_id' => Address::create($data)->id] : ['address_id' => $existingAddress->id]);
    }
}
