<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function transports()
    {
        return $this->hasMany(Transport::class);
    }

    public function drivers()
    {
        return $this->hasMany(Driver::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function prices()
    {
        return $this->hasMany(CarrierPrice::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function contacts()
    {
        return $this->hasMany(CarrierContact::class);
    }
}
