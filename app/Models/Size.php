<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function transports()
    {
        return $this->hasMany(Transport::class);
    }

    public function demands()
    {
        return $this->hasMany(Demand::class);
    }

    public function types()
    {
        return $this->belongsToMany(Type::class, 'size_type');
    }
}
