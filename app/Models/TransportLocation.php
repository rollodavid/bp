<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransportLocation extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function transport()
    {
        return $this->belongsTo(Transport::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
