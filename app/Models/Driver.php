<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function carrier()
    {
        return $this->belongsTo(Carrier::class);
    }

    public function transports()
    {
        return $this->belongsToMany(Transport::class); //hasMany
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }
}
