<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function carrier()
    {
        return $this->belongsTo(Carrier::class);
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function chassis()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function locations()
    {
        return $this->belongsToMany(Location::class, 'transport_locations');
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function transportLocations()
    {
        return $this->hasMany(TransportLocation::class);
    }
}
