<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    protected $guarded = [];

    use HasFactory;
    
    public function heights()
    {
        return $this->belongsToMany(VehicleHeight::class, 'vehicles', 'type_id', 'height_id');
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'type_id');
    }
}
