<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function transports()
    {
        return $this->hasMany(Transport::class);
    }

    public function demands()
    {
        return $this->hasMany(Demand::class);
    }

    public function sizes()
    {
        return $this->belongsToMany(Size::class, 'size_type');
    }
}
