<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarrierPrice extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function carrier()
    {
        return $this->belongsTo(Carrier::class);
    }

    public function demand()
    {
        return $this->belongsTo(Demand::class);
    }
}
