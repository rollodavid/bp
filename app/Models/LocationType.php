<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocationType extends Model
{
    protected $guarded = [];

    use HasFactory;

    public function addresses()
    {
        return $this->belongsToMany(Address::class, 'locations', 'type_id', 'address_id');
    }
}
