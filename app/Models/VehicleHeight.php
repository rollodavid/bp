<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleHeight extends Model
{
    protected $guarded = [];

    use HasFactory;
    
    public function types()
    {
        return $this->belongsToMany(VehicleType::class, 'vehicles', 'type_id', 'height_id');
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class, 'height_id');
    }
}
