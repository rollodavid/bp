<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use Auth;

class Pricing extends Notification
{
    public $subject;
    public $demand;
    public $pricing;
    public $note;
    public $cc;

    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct($subject, $demand, $pricing, $note, $cc)
    {
        $this->subject = $subject;
        $this->demand = $demand;
        $this->pricing = $pricing;
        $this->note = $note;
        $this->cc = $cc;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->from(env('APP_EMAIL'), Auth::user()->name)
            ->subject($this->subject)
            ->markdown('emails.customer.pricing', [
                'demand' => $this->demand,
                'pricing' => $this->pricing,
                'note' => $this->note,
                'username' => Auth::user()->name,
            ])
            ->cc($this->cc);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
