<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

use Auth;

class DemandInfo extends Notification
{
    public $subject;
    public $demand;
    public $note;

    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct($subject, $demand, $note)
    {
        $this->subject = $subject;
        $this->demand = $demand;
        $this->note = $note;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->from(Auth::user()->email)
            ->subject($this->subject)
            ->markdown('emails.carrier.demand_info', [
                'demand' => $this->demand,
                'note' => $this->note,
                'username' => Auth::user()->name,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
