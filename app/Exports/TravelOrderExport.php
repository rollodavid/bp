<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Cell\DataType;

use Carbon\Carbon;

class TravelOrderExport implements WithStyles
{
    public $transport;

    public function __construct($transport) {
        $this->transport = $transport;
    }

    public function styles(Worksheet $sheet) {
        
        $sheet->getCell('A1')->setValue('Jízdní příkaz');

        # Hlavička společnosti
        $sheet->getCell('A3')->setValue('ČD-DUSS Terminál, a.s.');
        $sheet->getCell('A4')->setValue('Lukavecká 1189');
        $sheet->getCell('A5')->setValue('402 02 Lovosice');

        # Datum
        $sheet->getCell('A7')->setValue('Datum:');
        if ($this->transport->date != null) $sheet->getCell('A8')->setValue(Carbon::createFromFormat('Y-m-d', $this->transport->date)->format('d.m.Y'));

        # Dopravce
        $sheet->getCell('B7')->setValue('Dopravce:');
        $sheet->getCell('B8')->setValue($this->transport->carrier->name);

        # PJ
        $sheet->getCell('D7')->setValue($this->transport->type->name . ' (' . $this->transport->size->value . ')');
        $sheet->getCell('D8')->setValue($this->transport->ilu);

        # Celní služby
        if ($this->transport->service_new) $sheet->getCell('D3')->setValue('Nová T1');
        if ($this->transport->service_free) $sheet->getCell('D3')->setValue('Propuštění do VO');
        $this->setAlignment($sheet, 'D3', 'D3', 'right');

        # Zalamování textů
        $this->setAlignment($sheet, 'A10', 'D50', 'left');
        $sheet->getStyle('A10:D50')->applyFromArray([
            'alignment' => [
                'wrapText' => true,
            ],
        ]);

        ## Trasa
        $columns = [
            '1' => 'A',
            '2' => 'B',
            '3' => 'C',
        ];

        $row = $last_row = $last_row_middle = 10;
        foreach ($this->transport->transportLocations as $key => $info) {
            if ($info->location->type->name == 'Vyzvednutí') {
                #typ
                $this->setBorder($sheet, 'A'.$row, 'A'.$row, 'allBorders');
                $this->setAlignment($sheet, 'A'.$row, 'A'.$row, 'center');
                $sheet->getCell('A' . $row++)->setValue($info->location->type->name);
                #adresa
                if ($info->company != "") {
                    $this->setBorder($sheet, 'A'.$row, 'A'.$row+2, 'outline');
                    $sheet->getCell('A' . $row++)->setValue($info->company);
                }
                else $this->setBorder($sheet, 'A'.$row, 'A'.$row+1, 'outline');

                $sheet->getCell('A' . $row++)->setValue($info->location->address->street . ' ' . $info->location->address->number);
                $sheet->getCell('A' . $row++)->setValue($info->location->address->psc . ' ' . $info->location->address->city);
                #reference
                if ($info->ref != null) {
                    $this->setBorder($sheet, 'A'.$row, 'A'.$row, 'allBorders');
                    $sheet->getCell('A' . $row++)->setValue('Ref: ' . $info->ref);
                }
                #netto
                $this->setBorder($sheet, 'A'.$row, 'A'.$row, 'allBorders');
                $sheet->getCell('A' . $row++)->setValue('Netto: ' . $this->transport->netto_pick . ' Kg');

                $last_row = $last_row_middle = $row + 1;
            }
            elseif ($info->location->type->name == 'Vrácení') {
                $row = $last_row_middle;

                #typ
                $this->setBorder($sheet, 'A'.$row, 'A'.$row, 'allBorders');
                $this->setAlignment($sheet, 'A'.$row, 'A'.$row, 'center');
                $sheet->getCell('A' . $row++)->setValue($info->location->type->name);
                #adresa
                if ($info->company != "") {
                    $this->setBorder($sheet, 'A'.$row, 'A'.$row+2, 'outline');
                    $sheet->getCell('A' . $row++)->setValue($info->company);
                }
                else $this->setBorder($sheet, 'A'.$row, 'A'.$row+1, 'outline');

                $sheet->getCell('A' . $row++)->setValue($info->location->address->street . ' ' . $info->location->address->number);
                $sheet->getCell('A' . $row++)->setValue($info->location->address->psc . ' ' . $info->location->address->city);
                #reference
                if ($info->ref != null) {
                    $this->setBorder($sheet, 'A'.$row, 'A'.$row, 'allBorders');
                    $sheet->getCell('A' . $row++)->setValue('Ref: ' . $info->ref);
                }

                $last_row = $row + 1;
            }
            else {
                $row = $last_row;

                #typ
                $this->setBorder($sheet, $columns[$key].$row, $columns[$key].$row, 'allBorders');
                $this->setAlignment($sheet, $columns[$key].$row, $columns[$key].$row, 'center');
                $sheet->getCell($columns[$key] . $row++)->setValue($info->location->type->name);
                #adresa
                if ($info->company != "") {
                    $this->setBorder($sheet, $columns[$key].$row, $columns[$key].$row+2, 'outline');
                    $sheet->getCell($columns[$key] . $row++)->setValue($info->company);
                }
                else $this->setBorder($sheet, $columns[$key].$row, $columns[$key].$row+1, 'outline');

                $sheet->getCell($columns[$key] . $row++)->setValue($info->location->address->street . ' ' . $info->location->address->number);
                $sheet->getCell($columns[$key] . $row++)->setValue($info->location->address->psc . ' ' . $info->location->address->city);
                #reference
                if ($info->ref != null) {
                    $this->setBorder($sheet, $columns[$key].$row, $columns[$key].$row, 'allBorders');
                    $sheet->getCell($columns[$key] . $row++)->setValue('Ref: ' . $info->ref);
                }
                #netto
                if ($info->location->type->name == 'Nakládka') {
                    $this->setBorder($sheet, $columns[$key].$row, $columns[$key].$row, 'allBorders');
                    $sheet->getCell($columns[$key] . $row++)->setValue('Netto: ' . $this->transport->netto_load . ' Kg');
                }
                #celní deklarace
                if ($info->location->type->name == 'Clení' && $this->transport->customs_contactname != "")
                {
                    $sheet->getCell($columns[$key] . $row++)->setValue('Celní deklarace:');
                    $sheet->getCell($columns[$key] . $row++)->setValue($this->transport->customs_contactname);
                    $sheet->getCell($columns[$key] . $row++)->setValue($this->transport->customs_email);
                    if ($this->transport->customs_tel != null) {
                        $this->setAlignment($sheet, $columns[$key] . $row, $columns[$key] . $row, 'left');
                        $sheet->getCell($columns[$key] . $row++)->setValueExplicit($this->transport->customs_tel, DataType::TYPE_STRING);
                        $this->setBorder($sheet, $columns[$key].($row-4), $columns[$key].($row-1), 'outline');
                    }
                    else $this->setBorder($sheet, $columns[$key].($row-3), $columns[$key].($row-1), 'outline');
                }

                $time_from = date("G:i", strtotime($info->from));
                $time_to = date("G:i", strtotime($info->to));
                if ($time_from == '0:00' && $time_to == '0:00') {
                    #nic
                }
                elseif (!($time_from == '0:00' && $time_to == '0:00')) {
                    $this->setBorder($sheet, $columns[$key] . $row, $columns[$key] . $row, 'outline');
                    $sheet->getCell($columns[$key] . $row++)->setValue($time_from . ' - ' . $time_to);
                }
                elseif ($time_from == '0:00') {
                    $this->setBorder($sheet, $columns[$key] . $row, $columns[$key] . $row, 'outline');
                    $sheet->getCell($columns[$key] . $row++)->setValue(' - ' . $time_to);
                }
                elseif ($time_to == '0:00') {
                    $this->setBorder($sheet, $columns[$key] . $row, $columns[$key] . $row, 'outline');
                    $sheet->getCell($columns[$key] . $row++)->setValue($time_from . ' - ');
                }
                
                if ($last_row_middle <= $row) $last_row_middle = $row + 1;
            }
        }
        $row = $last_row;

        $this->setAlignment($sheet, 'A' . $row, 'C' . $row, 'center');
        $this->setBorder($sheet, 'A' . $row, 'A' . $row, 'outline');
        $sheet->getCell('C' . $row)->setValue('Vyplnit CMR a nechat ho potvrdit na nakládce/vykládce.');
        $sheet->getStyle('C' . $row)->applyFromArray([
            'alignment' => [
                'wrapText' => false,
            ],
        ]);
        $sheet->getCell('A' . $row++)->setValue('Poznámka od přepravce');
        
        $sheet->mergeCells('A' . $row . ':D' . $row);
        $this->setBorder($sheet, 'A' . ($row-1), 'D' . $row, 'outline');
        $sheet->getRowDimension($row)->setRowHeight(30);
        $sheet->getStyle('A' . $row . ':D' . $row)->applyFromArray([
            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
            ],
        ]);
        $sheet->getCell('A' . $row++)->setValue($this->transport->travel_note);
        
        # SPs
        $columns = [
            '0' => 'A',
            '1' => 'B',
            '2' => 'C',
            '3' => 'D',
            '4' => 'E',
        ];
        foreach ($this->transport->transportLocations as $key => $info) {
            if ($key == 0) $this->setBorder($sheet, $columns[$key].($row + 1), $columns[$key].($row + 4), 'allBorders');
            if ($info->location->type->name != 'Vyzvednutí' && $info->location->type->name != 'Vrácení') {
                $this->setBorder($sheet, $columns[$key].($row + 1), $columns[$key].($row + 4), 'allBorders');
                $this->setAlignment($sheet, $columns[$key] . ($row + 1), $columns[$key] . ($row + 1), 'center');
                $sheet->getCell($columns[$key] . ($row + 1))->setValue($info->location->type->name);
            }
        }
        $row+=2;
        
        $sheet->getRowDimension($row)->setRowHeight(30);
        $this->setAlignment($sheet, 'A'.$row, 'A'.$row+2, 'center');
        $sheet->getStyle('A' . $row . ':A' . $row+2)->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ]);

        $sheet->getCell('A' . $row++)->setValue('Datum');
        $sheet->getRowDimension($row)->setRowHeight(30);
        $sheet->getCell('A' . $row++)->setValue('Čas příjezdu');
        $sheet->getRowDimension($row)->setRowHeight(30);
        $sheet->getCell('A' . $row++)->setValue('Čas odjezdu');

        $row+=2;
        
        $sheet->getCell('C' . $row)->setValue('Podpis řidiče:');
        $this->setAlignment($sheet, 'C' . $row, 'C' . $row, 'right');
        $sheet->getCell('D' . $row)->setValue('......................................');
        $this->setAlignment($sheet, 'D' . $row, 'D' . $row, 'center');
        

        $sheet->getStyle('A1:D' . $row)->applyFromArray([
            'font' => ['bold' => true],
        ]);

        $sheet->getColumnDimension('A')->setWidth(22.5);
        $sheet->getColumnDimension('B')->setWidth(22.5);
        $sheet->getColumnDimension('C')->setWidth(22.5);
        $sheet->getColumnDimension('D')->setWidth(22.5);
    }

    public function setBorder($sheet, $cell1, $cell2, $type) {
        $sheet->getStyle($cell1 . ':' . $cell2)->applyFromArray([
            'borders' => [
                $type => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ]);
    }

    public function setAlignment($sheet, $cell1, $cell2, $type) {
        if ($type == 'left') {
            $sheet->getStyle($cell1 . ':' . $cell2)->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                ],
            ]);
        }
        elseif ($type == 'center') {
            $sheet->getStyle($cell1 . ':' . $cell2)->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                ],
            ]);
        }
        elseif ($type == 'right') {
            $sheet->getStyle($cell1 . ':' . $cell2)->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
                ],
            ]);
        }
    }
}
