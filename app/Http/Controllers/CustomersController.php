<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Address;
use App\Models\Customer;
use App\Models\CustomerContact;
use App\Models\Transport;

class CustomersController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $customers = Customer::all()->sortBy(['name', 'asc']);
        return view('database.customers.customers', compact('customers'));
    }

    public function create() {
        return view('database.customers.create');
    }

    public function store() {
        $contact = request()->validate([
            'contactname' => 'required',
            'email' => 'required|email',
            'tel' => '',
        ]);
        
        CustomerContact::create([
            'customer_id' => Customer::create(request()->validate([
                'name' => 'required',
                'ico' => 'required|digits:8',
                'dic' => 'required|size:10',
            ]) + Address::addAddress(request()->validate([
                'city' => 'required',
                'street' => '',
                'number' => 'required',
                'psc' => 'required|digits:5',
            ])))->id,
            'contactname' => $contact['contactname'],
            'email' => $contact['email'],
            'tel' => $contact['tel'],
        ]);

        return redirect('/customers');
    }

    public function edit(Customer $customer) {
        return view('database.customers.edit', compact('customer'));
    }

    public function update(Customer $customer) {
        $customer->update(request()->validate([
            'name' => 'required',
            'ico' => 'required|digits:8',
            'dic' => 'required|size:10',
        ]) + Address::addAddress(request()->validate([
            'city' => 'required',
            'street' => '',
            'number' => 'required',
            'psc' => 'required|digits:5',
        ])));
        
        $contacts = request()->validate([
            'contactname' => 'required',
            'email' => 'required',
            'tel' => '',
        ]);

        for ($i=0; $i < count($customer->contacts); $i++) {
            $customer->contacts[$i]->contactname = $contacts['contactname'][$i];
            $customer->contacts[$i]->email = $contacts['email'][$i];
            $customer->contacts[$i]->tel = $contacts['tel'][$i];
            $customer->contacts[$i]->save();
        }

        if (count(request()->contactname) > count($customer->contacts)) {
            for ($i=count($customer->contacts); $i < count(request()->contactname); $i++) {
                if ($contacts['contactname'][$i] != null && $contacts['email'][$i] != null) {
                    CustomerContact::create([
                        'customer_id' => $customer->id,
                        'contactname' => $contacts['contactname'][$i],
                        'email' => $contacts['email'][$i],
                        'tel' => $contacts['tel'][$i],
                    ]);
                }
            }
        }

        return redirect('/customers');
    }

    public function destroy(Customer $customer) {
        $amount = Transport::where('customer_id', $customer->id)->get()->count();
        
        if ($amount > 0) {
            return redirect()->back()->with('alert', 'Smazání selhalo!\nPočet aktivních přeprav: ' . $amount);
        }

        /* //Přepsání na neznámého zákazníka
        //$unknown_customer = Customer::where('name', 'Neznámý zákazník')->first();
        foreach ($transports as $transport) {
            $transport->customer_id = $unknown_customer->id;
            $transport->save();
        }
        */
        
        Customer::destroy($customer->id);

        return redirect('/customers');
    }
}
