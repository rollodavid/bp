<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Notification;
use App\Notifications\DemandInfo;
use App\Notifications\Pricing;

use App\Models\CarrierPrice;
use App\Models\Demand;
use App\Models\User;

class EmailsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index_demand_info($demand_id) {
        $demand = Demand::findOrFail($demand_id);
        return view('emails.carrier.send', compact('demand'));
    }

    public function index_pricing($demand_id, $carrierPrice_id) {
        $demand = Demand::findOrFail($demand_id);
        $pricing = CarrierPrice::findOrFail($carrierPrice_id);
        return view('emails.customer.send', compact('demand','pricing'));
    }

    public function send_demand_info() {
        $data = request()->validate([
            'to' => 'required',
            'demand_id' => 'required',
            'subject' => 'required',
            'note' => '',
        ]);

        $demand = Demand::find($data['demand_id']);
        $note = $data['note'];

        foreach (explode(', ', $data['to']) as $receiver_email) {
            $receiver = new User();
            $receiver->email = $receiver_email;
            Notification::send($receiver, new DemandInfo($data['subject'], $demand, $note));
        }
        
        return redirect('demands/' . $demand->id . '/edit');
    }

    public function send_pricing() {
        $data = request()->validate([
            'to' => 'required',
            'cc' => '',
            'demand_id' => 'required',
            'pricing_id' => 'required',
            'subject' => 'required',
            'note' => '',
        ]);

        $demand = Demand::find($data['demand_id']);
        $pricing = CarrierPrice::find($data['pricing_id']);
        $note = $data['note'];
        $cc = [];

        foreach (explode(', ', $data['cc']) as $cc_email) {
            if ($cc_email != "") array_push($cc, trim($cc_email, ','));
        }

        foreach (explode(', ', $data['to']) as $receiver_email) {
            $receiver = new User();
            $receiver->email = $receiver_email;
            Notification::send($receiver, new Pricing($data['subject'], $demand, $pricing, $note, $cc));
        }
        
        return redirect('demands/' . $demand->id . '/edit');
    }
}
