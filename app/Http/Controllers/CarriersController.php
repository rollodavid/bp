<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Address;
use App\Models\Carrier;
use App\Models\CarrierContact;
use App\Models\Driver;
use App\Models\Transport;
use App\Models\Vehicle;

class CarriersController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $carriers = Carrier::all()->sortBy(['name', 'asc']);
        
        $chassises = [];
        foreach ($carriers as $carrier) {
            array_push($chassises, Vehicle::where('carrier_id', $carrier->id)->where('type_id', 2)->get()->count());
        }

        return view('database.carriers.carriers', compact('carriers','chassises'));
    }

    public function create() {
        return view('database.carriers.create');
    }

    public function store() {
        $contact = request()->validate([
            'contactname' => 'required',
            'email' => 'required',
            'tel' => '',
        ]);

        CarrierContact::create([
            'carrier_id' => Carrier::create(request()->validate([
                'name' => 'required',
                'ico' => 'required|digits:8',
                'dic' => 'required|size:10',
            ]) + Address::addAddress(request()->validate([
                'city' => 'required',
                'street' => '',
                'number' => 'required',
                'psc' => 'required|digits:5',
            ])))->id,
            'contactname' => $contact['contactname'],
            'email' => $contact['email'],
            'tel' => $contact['tel'],
        ]);

        return redirect('/carriers');
    }

    public function edit(Carrier $carrier) {
        $drivers = Driver::where('carrier_id', $carrier->id)->get();
        $chassises = Vehicle::where('carrier_id', $carrier->id)->where('type_id', 2)->get();
        return view('database.carriers.edit', compact('carrier','drivers','chassises'));
    }

    public function update(Carrier $carrier) {
        $carrier->update(request()->validate([
            'name' => 'required',
            'ico' => 'required|digits:8',
            'dic' => 'required|size:10',
        ]) + Address::addAddress(request()->validate([
            'city' => 'required',
            'street' => '',
            'number' => 'required',
            'psc' => 'required|digits:5',
        ])));

        $contacts = request()->validate([
            'contactname' => 'required',
            'email' => 'required',
            'tel' => '',
        ]);

        for ($i=0; $i < count($carrier->contacts); $i++) {
            $carrier->contacts[$i]->contactname = $contacts['contactname'][$i];
            $carrier->contacts[$i]->email = $contacts['email'][$i];
            $carrier->contacts[$i]->tel = $contacts['tel'][$i];
            $carrier->contacts[$i]->save();
        }

        if (count(request()->contactname) > count($carrier->contacts)) {
            for ($i=count($carrier->contacts); $i < count(request()->contactname); $i++) {
                if ($contacts['contactname'][$i] != null && $contacts['email'][$i] != null) {
                    CarrierContact::create([
                        'carrier_id' => $carrier->id,
                        'contactname' => $contacts['contactname'][$i],
                        'email' => $contacts['email'][$i],
                        'tel' => $contacts['tel'][$i],
                    ]);
                }
            }
        }

        return redirect('/carriers');
    }

    public function destroy(Carrier $carrier) {
        $amount = Transport::where('carrier_id', $carrier->id)->get()->count();
        
        if ($amount > 0) {
            return redirect()->back()->with('alert', 'Smazání selhalo!\nPočet aktivních přeprav: ' . $amount);
        }

        /* //Přepsání na neznámého zákazníka
        //$unknown_carrier = carrier::where('name', 'Neznámý zákazník')->first();
        foreach ($transports as $transport) {
            $transport->carrier_id = $unknown_carrier->id;
            $transport->save();
        }
        */
        
        Carrier::destroy($carrier->id);

        return redirect('/carriers');
    }
}
