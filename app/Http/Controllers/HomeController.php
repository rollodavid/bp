<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Address;
use App\Models\Carrier;
use App\Models\Customer;
use App\Models\Demand;
use App\Models\Driver;
use App\Models\Location;
use App\Models\Size;
use App\Models\Transport;
use App\Models\Type;
use App\Models\User;
use App\Models\Vehicle;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $users = User::all();
        $transports = Transport::all();
        $demands = Demand::all();
        $types = Type::all();
        $sizes = Size::all();
        $customers = Customer::all();
        $carriers = Carrier::all();
        $drivers = Driver::all();
        $trucks = Vehicle::where('type_id', 1)->get();
        $chassises = Vehicle::where('type_id', 2)->get();
        $addresses = Address::all();
        return view('home', compact('users','customers','carriers','drivers','transports','demands','types','sizes','addresses','chassises','trucks'));
    }
}
