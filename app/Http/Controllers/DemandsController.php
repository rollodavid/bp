<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Notification;
use App\Notifications\NewDemand;

use App\Models\Address;
use App\Models\Demand;
use App\Models\Location;
use App\Models\LocationType;
use App\Models\Size;
use App\Models\Type;
use App\Models\User;

class DemandsController extends Controller
{
    public function confirm() {
        return view('pricelist.demand.confirm');
    }

    public function create() {
        $types = Type::all();
        $sizes = Size::all();
        return view('pricelist.demand.create', compact('types','sizes'));
    }

    public function store() {
        
        # Data validation
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'type_id' => 'required',
            'size_id' => 'required',
            'netto_pick' => 'required',
            'netto_load' => '',
            'customs_contactname' => '',
            'customs_email' => '',
            'customs_tel' => '',
            'note' => '',
        ]);

        //Services
        $data = $data + ['service_new' => (request()->service_new == null ? false : true)];
        $data = $data + ['service_end' => (request()->service_end == null ? false : true)];
        $data = $data + ['service_free' => (request()->service_free == null ? false : true)];

        # Route validation
        $pathValidation = request()->validate([
            'type' => 'required',
            'city' => 'required',
            'street' => '',
            'number' => 'required',
            'psc' => 'required|digits:5',
        ]);

        //Min 2 locations
        if (count($pathValidation['type']) < 2)
            return redirect()->back()->with('alert','Trasa musí mít minimálně dvě lokace!');
        //Duplicity
        if (count($pathValidation['type']) != count(array_unique($pathValidation['type'])))
            return redirect()->back()->with('alert','Dvě stejné lokace!');
        //Place of return && pick-up
        if (!(in_array('Vyzvednutí', $pathValidation['type']) && in_array('Vrácení', $pathValidation['type'])))
            return redirect()->back()->with('alert','V trase musí být lokace pro vyzvednutí PJ a lokace pro vrácení PJ!');

        # Demand creating
        $demand = Demand::create($data);

        # Route saving
        $type = request()->type;
        if ($type != null) {
            foreach ($type as $key => $value) {
                $typeValidation = request()->validate([
                    'type' => 'required',
                ]);
                $addressValidation = request()->validate([
                    'city' => 'required',
                    'street' => '',
                    'number' => 'required',
                    'psc' => 'required|digits:5',
                ]);

                $type = LocationType::where('name',$typeValidation['type'][$key])->first();
                $existingAddress = Address::where('city',$addressValidation['city'][$key])
                    ->where('street',$addressValidation['street'][$key])
                    ->where('number',$addressValidation['number'][$key])
                    ->where('psc',$addressValidation['psc'][$key])->first();

                if ($existingAddress == null) { # New address
                    $newAddress = new Address();
                    $newAddress->city = $addressValidation['city'][$key];
                    $newAddress->street = $addressValidation['street'][$key];
                    $newAddress->number = $addressValidation['number'][$key];
                    $newAddress->psc = $addressValidation['psc'][$key];
                    $newAddress->save();
                    $newAddress->types()->sync($type->id);
                }
                else { # Old address
                    $existingLocation = Location::where('address_id', $existingAddress->id)->where('type_id', $type->id)->first();

                    if ($existingLocation == null) { # Old address with new type
                        $existingAddress->types()->attach($type->id);
                    }
                }

                $existingLocation = Location::where('address_id', $existingAddress->id ?? $newAddress->id)->where('type_id', $type->id)->first();
                $existingLocation->demands()->attach($demand->id);
            }
        }

        $receiver = new User();
        $receiver->email = env('APP_EMAIL');
        Notification::send($receiver, new NewDemand($demand));
        
        return view('pricelist.demand.confirm');
    }
}
