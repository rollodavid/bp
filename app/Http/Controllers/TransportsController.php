<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Models\Carrier;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\File;
use App\Models\Location;
use App\Models\Size;
use App\Models\Transport;
use App\Models\Type;
use App\Models\Vehicle;

use App\Exports\TravelOrderExport;
use Maatwebsite\Excel\Facades\Excel;

class TransportsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index() {
        $transports = Transport::all()->sortDesc();
        return view('transports.transports', compact('transports'));
    }

    public function edit(Transport $transport) {
        $customers = Customer::all();
        $carriers = Carrier::all();
        $drivers = Driver::where('carrier_id', $transport->carrier->id)->get();
        $chassises = Vehicle::where('carrier_id', $transport->carrier->id)->where('type_id', 2)->get(); //2 = Šasí
        $types = Type::all();
        $sizes = Size::all();
        $files = File::where('transport_id', $transport->id)->get();
        return view('transports.edit', compact('transport', 'customers','carriers','drivers','chassises','types','sizes','files'));
    }

    public function update(Transport $transport) {
        $data = request()->validate([
            'ilu' => '',
            'customer_id' => 'required',
            'date' => '',
            'carrier_id' => 'required',
            'driver_id' => '',
            'chassis_id' => '',
            'type_id' => 'required',
            'size_id' => 'required',
            'netto_pick' => 'required',
            'netto_load' => '',
            'carrier_price' => '',
            'free_hours' => '',
            'waiting_price_by_hour' => '',
            'waiting_hours' => '',
            'waiting' => '',
            'percentage' => '',
            'carrier_total' => '',
            'customs_contactname' => '',
            'customs_email' => '',
            'customs_tel' => '',
            'service_new' => '',
            'service_end' => '',
            'service_free' => '',
            'service_price' => '',
            'total' => '',
            'travel_note' => '',
        ]);

        if ($data['date'] == null) $data['date'] = null;

        $data['travel_note'] = trim(preg_replace('/\s+/', ' ', $data['travel_note']));

        # Services
        if (request()->service_new == null) $data = $data + ['service_new' => false];
        else $data['service_new'] = true;
        if (request()->service_end== null) $data = $data + ['service_end' => false];
        else $data['service_end'] = true;
        if (request()->service_free== null) $data = $data + ['service_free' => false];
        else $data['service_free'] = true;

        $transport->update($data);

        //Firmy, reference, datumy a časy

        $data = request()->validate([
            'company' => '',
            'ref' => '',
            'location_date' => '',
            'from' => '',
            'to' => '',
        ]);
        foreach ($transport->transportLocations as $key => $info) {
            $info->company = $data['company'][$key];

            if ($info->location->type->name == 'Vyzvednutí' || $info->location->type->name == 'Vykládka' || $info->location->type->name == 'Nakládka' || $info->location->type->name == 'Vrácení') {
                $info->ref = $data['ref'][$key];
            }
            if ($info->location->type->name == 'Clení' || $info->location->type->name == 'Vykládka' || $info->location->type->name == 'Nakládka') {
                $info->location_date = $data['location_date'][$key];
                $info->from = $data['from'][$key];
                $info->to = $data['to'][$key];
            }
            $info->save();
        }

        //Soubory
        request()->validate([
            'file' => 'mimes:pdf,xlx,xlsx,xls,csv,jpg,png,jpeg',
        ]);

        if (request()->file != null) {
            $filePath = request()->file->store('uploads', 'local');

            File::create([
                'name' => request()->file->getClientOriginalName(),
                'path' => $filePath,
                'transport_id' => $transport->id,
            ]);
        }

        return redirect()->route('transports.edit', [$transport->id]);
    }

    public function destroy(Transport $transport) {
        Transport::destroy($transport->id);
        return redirect('/transports');
    }

    public function download($file_id) {
        $file = File::where('id', $file_id)->first();
        return response()->download(Storage::path($file->path), $file->name);
    }

public function export($transport_id) {
    $transport = Transport::where('id', $transport_id)->first();
    return Excel::download(new TravelOrderExport($transport), $transport->ilu . '_JP.xlsx');
}
}