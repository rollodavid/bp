<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Carrier;
use App\Models\Vehicle;
use App\Models\VehicleHeight;
use App\Models\VehicleType;

class VehiclesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $vehicles = Vehicle::all();
        return view('database.vehicle.vehicles', compact('vehicles'));
    }

    public function create() {
        $heights = VehicleHeight::all();
        $types = VehicleType::all();
        $carriers = Carrier::all();
        return view('database.vehicle.create', compact('heights','types','carriers'));
    }

    public function store() {
        Vehicle::create(request()->validate([
            'rz' => 'required',
            'type_id' => 'required',
            'height_id' => 'required',
            'carrier_id' => 'required',
        ]));

        return redirect('/vehicles');
    }

    public function edit(Vehicle $vehicle) {
        $heights = VehicleHeight::all();
        $types = VehicleType::all();
        $carriers = Carrier::all();
        return view('database.vehicle.edit', compact('vehicle','heights','types','carriers'));
    }

    public function update(Vehicle $vehicle) {
        $vehicle->update(request()->validate([
            'rz' => 'required',
            'type_id' => 'required',
            'height_id' => 'required',
            'carrier_id' => 'required',
        ]));

        return redirect('/vehicles');
    }

    public function destroy(Vehicle $vehicle) {
        Vehicle::destroy($vehicle->id);
        return redirect('/vehicles');
    }
}
