<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Transport;
use App\Models\Type;

class TypesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        return view('database.types.create');
    }

    public function store() {
        Type::create(request()->validate([
            'name' => 'required',
        ]));

        return redirect('/sizes');
    }

    public function edit(Type $type) {
        return view('database.types.edit', compact('type'));
    }

    public function update(Type $type) {
        $type->update(request()->validate([
            'name' => 'required',
        ]));

        return redirect('/sizes');
    }

    public function destroy(Type $type) {
        $amount = Transport::where('type_id', $type->id)->get()->count();
        
        if ($amount > 0) {
            return redirect()->back()->with('alert', 'Smazání selhalo!\nPočet aktivních přeprav: ' . $amount);
        }
        
        Type::destroy($type->id);

        return redirect('/sizes');
    }
}
