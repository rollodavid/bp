<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Address;
use App\Models\Carrier;
use App\Models\CarrierPrice;
use App\Models\Customer;
use App\Models\Demand;
use App\Models\Location;
use App\Models\LocationType;
use App\Models\Size;
use App\Models\Transport;
use App\Models\Type;

class PriceListController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index() {
        $data = Demand::all()->sortDesc();
        return view('pricelist.pricelist', compact('data'));
    }

    public function edit(Demand $demand) {
        $customers = Customer::all();
        $carriers = Carrier::all();
        $types = Type::all();
        $sizes = Size::all();
        $prices = CarrierPrice::where('demand_id', $demand->id)->get();
        return view('pricelist.edit', compact('demand','types','sizes','customers','carriers','prices'));
    }

    public function update(Demand $demand) {
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required',
            'customer_id' => '',
            'type_id' => 'required',
            'size_id' => 'required',
            'netto_pick' => 'required',
            'netto_load' => '',
            'service_price' => '',
            'customs_contactname' => '',
            'customs_email' => '',
            'customs_tel' => '',
        ]);

        # Services
        $data = $data + ['service_new' => (request()->service_new == null ? false : true)];
        $data = $data + ['service_end' => (request()->service_end == null ? false : true)];
        $data = $data + ['service_free' => (request()->service_free == null ? false : true)];

        $demand->update($data);

        //Ukládání dopravců a cen
        if (request()->carrier_id != null) {
            foreach (request()->carrier_id as $key => $value) {
                $data = request()->validate([
                    'carrier_id' => 'required',
                    'carrier_price' => '',
                    'free' => '',
                    'hour' => '',
                    'percentage' => '',
                    'carrier_total' => '',
                ]);
    
                $existingPrice = CarrierPrice::where('demand_id', $demand->id)->where('carrier_id', $value)->first();
    
                if ($existingPrice != null) {
                    $existingPrice->carrier_price = request()->carrier_price[$key];
                    $existingPrice->free = request()->free[$key];
                    $existingPrice->hour = request()->hour[$key];
                    $existingPrice->percentage = request()->percentage[$key];
                    $existingPrice->carrier_total = request()->carrier_total[$key];
                    $existingPrice->save();
                }
                else {
                    $newPrice = new CarrierPrice();
                    $newPrice->demand_id = $demand->id;
                    $newPrice->carrier_id = $value;
                    $newPrice->carrier_price = request()->carrier_price[$key];
                    $newPrice->free = request()->free[$key];
                    $newPrice->hour = request()->hour[$key];
                    $newPrice->percentage = request()->percentage[$key];
                    $newPrice->carrier_total = request()->carrier_total[$key];
                    $newPrice->save();
                }
            }
        }

        return redirect()->back();
    }

    public function realize(Demand $demand, Carrier $carrier) {
        $transport = new Transport();
        //FK
        $transport->customer_id = $demand->customer_id;
        $transport->carrier_id = $carrier->id;
        $transport->type_id = $demand->type_id;
        $transport->size_id = $demand->size_id;
        //PJ
        $transport->netto_pick = $demand->netto_pick;
        $transport->netto_load = $demand->netto_load;
        //Nacenění přepravy
        $pricing = CarrierPrice::where('demand_id', $demand->id)->where('carrier_id', $carrier->id)->first();
        $transport->carrier_price = $pricing->carrier_price;
        $transport->free_hours = $pricing->free;
        $transport->waiting_price_by_hour = $pricing->hour;
        $transport->percentage = $pricing->percentage;
        $transport->carrier_total = $pricing->carrier_total;
        //Celní deklarace
        $transport->customs_contactname = $demand->customs_contactname;
        $transport->customs_email = $demand->customs_email;
        $transport->customs_tel = $demand->customs_tel;
        //Celní služby
        $transport->service_new = $demand->service_new;
        $transport->service_end = $demand->service_end;
        $transport->service_free = $demand->service_free;
        $transport->service_price = $demand->service_price;
        //Celková cena
        $transport->total = ($pricing->carrier_total + $demand->service_price);
        
        $transport->save();

        //Trasa
        foreach ($demand->locations as $location) {
            $transport->locations()->attach($location);
        }

        return redirect('/transports' . '/' . $transport->id . '/edit');
    }
}
