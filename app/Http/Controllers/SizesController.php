<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Size;
use App\Models\Transport;
use App\Models\Type;

class SizesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $types = Type::all();
        $sizes = Size::all();
        return view('database.sizes.sizes', compact('sizes','types'));
    }

    public function create() {
        $types = Type::all();
        return view('database.sizes.create', compact('types'));
    }

    public function store() {
        $data = request()->validate([
            'value' => 'required',
        ]);

        $newSize = Size::create($data)->types()->sync(request()->type);

        return redirect('/sizes');
    }

    public function edit(Size $size) {
        $types = Type::all();
        return view('database.sizes.edit', compact('size','types'));
    }

    public function update(Size $size) {
        $data = request()->validate([
            'value' => 'required',
        ]);

        $size->update($data);

        $size->types()->sync(request()->type);
        //attach -> přidá do sizes() hodnotu v závorkách
        //sync -> nastaví sizes() na hodoty v závorkách

        return redirect('/sizes');
    }

    public function destroy(Size $size) {
        $amount = Transport::where('type_id', $size->id)->get()->count();
        
        if ($amount > 0) {
            return redirect()->back()->with('alert', 'Smazání selhalo!\nPočet aktivních přeprav: ' . $amount);
        }
        
        Size::destroy($size->id);

        return redirect('/sizes');
    }
}
