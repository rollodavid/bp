<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Carrier;
use App\Models\Driver;
use App\Models\Transport;
use App\Models\Vehicle;

class DriverController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function create() {
        $carriers = Carrier::all();
        $vehicles = Vehicle::where('type_id', 1)->get(); //1 = Tahač
        return view('database.carriers.drivers.create', compact('carriers','vehicles'));
    }

    public function store() {
        Driver::create(request()->validate([
            'name' => 'required',
            'carrier_id' => 'required',
            'vehicle_id' => 'required',
            'tel' => '',
        ]));

        return redirect('/carriers' . '/' . request()->carrier_id . '/edit');
    }

    public function edit(Driver $driver) {
        $carriers = Carrier::all();
        $vehicles = Vehicle::where('type_id', 1)->get(); //1 = Tahač
        return view('database.carriers.drivers.edit', compact('driver','carriers','vehicles'));
    }

    public function update(Driver $driver) {
        $driver->update(request()->validate([
            'name' => 'required',
            'carrier_id' => 'required',
            'vehicle_id' => 'required',
            'tel' => '',
        ]));

        return redirect('/carriers' . '/' . request()->carrier_id . '/edit');
    }

    public function destroy(Driver $driver) {
        $amount = Transport::where('driver_id', $driver->id)->get()->count();
        
        if ($amount > 0) {
            return redirect()->back()->with('alert', 'Smazání selhalo!\nPočet aktivních přeprav: ' . $amount);
        }
        
        Driver::destroy($driver->id);

        return redirect('/carriers');
    }
}
