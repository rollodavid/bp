<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Carrier;

class DemandCarrier extends Component
{
    public $carrier_id, $carrier_price, $free, $hour, $percentage, $carrier_total;
    public $carriers;
    public $inputs = [];
    public $i = 0;

    public function add($i) {
        if (count($this->inputs) < ($this->carriers==null ? 5 : count($this->carriers))) {
            $i = $i + 1;
            $this->i = $i;
            array_push($this->inputs, $i);
        }
    }

    public function remove($i) {
        unset($this->inputs[$i]);
    }

    public function fetchall() {
        $this->carriers = Carrier::all();
    }
}
