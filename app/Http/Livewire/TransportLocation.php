<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use App\Models\Location;

class TransportLocation extends Component
{
    public $type, $city, $street, $number, $psc;
    public $inputs = [];
    public $i = 1;

    public function add($i) {
        if (count($this->inputs) < 5) {
            $i = $i + 1;
            $this->i = $i;
            array_push($this->inputs, $i);
        }
    }

    public function remove($i) {
        unset($this->inputs[$i]);
    }
}
