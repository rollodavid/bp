@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Dopravní prostředky</h3>
    <div class="container d-flex gap-4">
        <div class="w-100">
            <table class="table mt-4" style="text-align: center; margin: 0 auto; width: 100%;">
                <thead>
                    <th>RZ</th>
                    <th>Typ</th>
                    <th>Výškový profil</th>
                    <th>Dopravce</th>
                    <th><a href="/vehicles/create" class="btn btn-primary"><b>+</b></a></th>
                </thead>
                <tbody>
                    @foreach ($vehicles as $vehicle)
                        <tr>
                            <td>{{ $vehicle->rz }}</td>
                            <td>{{ $vehicle->type->name }}</td>
                            <td>{{ $vehicle->height->name }}</td>
                            <td>{{ $vehicle->carrier->name }}</td>
                            <td><a href="/vehicles/{{ $vehicle->id }}/edit" class="btn btn-primary">Upravit</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
