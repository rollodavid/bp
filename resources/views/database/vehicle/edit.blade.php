@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Editace dopravního prostředku</h3>
    <form action="/vehicles/{{ $vehicle->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PATCH')
        
        <div class="col-4 offset-4">
            <!-- RZ -->
            <div class="row mb-3 mt-3">
                <label for="rz" class="col-form-label">RZ</label>
                <input id="rz"
                    type="text"
                    class="form-control @error('rz') is-invalid @enderror"
                    name="rz" value="{{ old('rz') ?? $vehicle->rz }}"
                    autocomplete="rz" autofocus required>
                @error('rz')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Type -->
            <div class="row mb-3">
                <label for="type_id" class="col-md-4 col-form-label">Typ prostředku</label>
                <select name="type_id" id="type_id" class="form-select" required>
                    <option value=""></option>
                    @foreach($types as $type)
                        <option value={{ $type->id }} @if($type->id==($vehicle->type->id ?? false)) selected='selected' @endif>{{ $type->name }}</option>
                    @endforeach
                </select>
                @error('type_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Height -->
            <div class="row mb-3">
                <label for="height_id" class="col-md-4 col-form-label w-100">Výška prostředku</label>
                <select name="height_id" id="height_id" class="form-select" required>
                    <option value=""></option>
                    @foreach($heights as $height)
                        <option value={{ $height->id }} @if($height->id==($vehicle->height->id ?? false)) selected='selected' @endif>{{ $height->name }}</option>
                    @endforeach
                </select>
                @error('height_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Carriers -->
            <div class="row mb-3">
                <label for="carrier_id" class="col-md-4 col-form-label">Dopravce</label>
                <select name="carrier_id" id="carrier_id" class="form-select" required>
                    <option value=""></option>
                    @foreach($carriers as $carrier)
                        <option value={{$carrier->id}} @if($carrier->id==($vehicle->carrier->id ?? false)) selected='selected' @endif>{{ $carrier->name }}</option>
                    @endforeach
                </select>
                @error('carrier_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Add -->
            <div class="row mt-3 justify-content-end">
                <button class="btn btn-primary w-auto">Uložit</button>
            </div>
        </div>
    </form>

    <form action="/vehicles/{{ $vehicle->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('DELETE')

        <div class="col-4 offset-4">
            <div class="row mt-3 justify-content-end gap-2">
                <button class="btn btn-danger w-auto" onclick="return confirm('Are you sure?')">Smazat</button>
                <a href="/vehicles" class="btn btn-secondary w-auto" onclick="return confirm('Are you sure?')">Zrušit</a>
            </div>
        </div>
    </form>
</div>
@endsection