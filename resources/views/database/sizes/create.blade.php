@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Nová velikost</h3>
    <form action="/sizes" enctype="multipart/form-data" method="POST">
        @csrf
        
        <div class="col-4 offset-4">
            <!-- Name -->
            <div class="row mb-3 mt-3">
                <label for="value" class="col-form-label">Velikost</label>
                <input id="value"
                    type="text"
                    class="form-control @error('value') is-invalid @enderror"
                    name="value" value="{{ old('value') }}"
                    autocomplete="value" autofocus required>
                @error('value')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Types (checkboxy) -->
            <div class="row mb-3 mt-3">
                @foreach($types as $type)
                    <div class="form-check ms-4">
                        <label for="{{$type->id}}" class="form-check-label">{{$type->name}}</label>
                        <input id="{{$type->id}}"
                            type="checkbox"
                            value="{{$type->id}}"
                            class="form-check-input"
                            name="type[]" value="{{ old('service_new') }}"
                            autocomplete="{{$type->id}}" autofocus>
                    </div>
                @endforeach
            </div>
            <!-- Add -->
            <div class="row mt-3 justify-content-end">
                <button class="btn btn-primary w-auto">Přidat</button>
            </div>
        </div>
    </form>
</div>
@endsection