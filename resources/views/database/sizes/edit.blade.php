@extends('layouts.app')

@section('content')
<div class="container">

    <h3>Editace typu</h3>

    <form action="/sizes/{{ $size->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PATCH')
        
        <div class="col-4 offset-4">
            <!-- Name -->
            <div class="row mb-3 mt-3">
                <label for="value" class="col-form-label">Velikost</label>
                <input id="value"
                    type="text"
                    class="form-control @error('value') is-invalid @enderror"
                    name="value" value="{{ old('value') ?? $size->value }}"
                    autocomplete="value" autofocus required>
                @error('value')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Types (checkboxy) -->
            <div class="row mb-3 mt-3">
                @foreach($types as $type)
                    <div class="form-check ms-4">
                        <label for="{{$type->id}}" class="form-check-label">{{$type->name}}</label>
                        <input id="{{$type->id}}"
                            type="checkbox"
                            value="{{$type->id}}"
                            class="form-check-input @error('service_new') is-invalid @enderror"
                            name="type[]" value="{{ old('service_new') }}"
                            autocomplete="{{$type->id}}" autofocus
                            {{ $size->types->contains($type->id) ? 'checked' : '' }}>
                        @error('service_new')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                @endforeach
            </div>
            <!-- Save -->
            <div class="row mt-3 justify-content-end">
                <button class="btn btn-primary w-auto">Uložit</button>
            </div>
        </div>
    </form>

    <form action="/sizes/{{ $size->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('DELETE')

        <div class="col-4 offset-4">
            <div class="row mt-3 justify-content-end gap-2">
                <button class="btn btn-danger w-auto" onclick="return confirm('Are you sure?')">Smazat</button>
                <a href="/sizes" class="btn btn-secondary w-auto" onclick="return confirm('Are you sure?')">Zrušit</a>
            </div>
        </div>
    </form>
</div>

<script>
    //Při mazání zákazníka se mazání přeruší, pokud má přidělenou nějakou přepravu
    //Script ukáže uživateli počet přidělených přeprav
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
</script>
@endsection
