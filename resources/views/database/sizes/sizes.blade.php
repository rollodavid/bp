@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Velikosti a typy PJ</h3>
    <div class="container d-flex gap-4">
        <div class="w-100">
            <table class="table mt-4" style="text-align: center; margin: 0 auto; width: 100%;">
                <thead>
                    <th>Typ</th>
                    <th><a href="/types/create" class="btn btn-primary"><b>+</b></a></th>
                </thead>
                <tbody>
                    @foreach ($types as $type)
                        <tr>
                            <td>{{ $type->name }}</td>
                            <td><a href="/types/{{ $type->id }}/edit" class="btn btn-primary">Upravit</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="w-100">
            <table class="table mt-4" style="text-align: center; margin: 0 auto; width: 100%;">
                <thead>
                    <th>Velikost</th>
                    <th>Typy</th>
                    <th><a href="/sizes/create" class="btn btn-primary"><b>+</b></a></th>
                </thead>
                <tbody>
                    @foreach ($sizes as $size)
                        <tr>
                            <td>{{ $size->value }}</td>
                            <td>
                                @for ($i = 0; $i < count($size->types); $i++)
                                    @if ($i+1 == count($size->types))
                                        {{$size->types[$i]->name}}
                                    @else
                                        {{$size->types[$i]->name}}, 
                                    @endif
                                @endfor
                            </td>
                            <td><a href="/sizes/{{ $size->id }}/edit" class="btn btn-primary">Upravit</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
