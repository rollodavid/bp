@extends('layouts.app')

@section('content')
<div class="container">

    <h3>Editace typu</h3>

    <form action="/types/{{ $type->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PATCH')
        
        <div class="col-4 offset-4">
            <!-- Name -->
            <div class="row mb-3 mt-3">
                <label for="name" class="col-form-label">Název typu</label>
                <input id="name"
                    type="text"
                    class="form-control @error('name') is-invalid @enderror"
                    name="name" value="{{ old('name') ?? $type->name }}"
                    autocomplete="name" autofocus required>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Save -->
            <div class="row mt-3 justify-content-end">
                <button class="btn btn-primary w-auto">Uložit</button>
            </div>
        </div>
    </form>

    <form action="/types/{{ $type->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('DELETE')

        <div class="col-4 offset-4">
            <div class="row mt-3 justify-content-end gap-2">
                <button class="btn btn-danger w-auto" onclick="return confirm('Are you sure?')">Smazat</button>
                <a href="/types" class="btn btn-secondary w-auto" onclick="return confirm('Are you sure?')">Zrušit</a>
            </div>
        </div>
    </form>
</div>
@endsection
