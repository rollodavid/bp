@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Nový typ</h3>
    <form action="/types" enctype="multipart/form-data" method="POST">
        @csrf
        
        <div class="col-4 offset-4">
            <!-- Jméno -->
            <div class="row mb-3">
                <label for="name" class="col-md-4 col-form-label">Název typu</label>
                <input id="name"
                    type="text"
                    class="form-control @error('name') is-invalid @enderror"
                    name="name" value="{{ old('name') }}"
                    autocomplete="name" autofocus required>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Uložit -->
            <div class="row mt-4 justify-content-end">
                <button class="btn btn-primary w-auto">Vytvořit</button>
            </div>
        </div>
    </form>
</div>
@endsection
