@extends('layouts.app')

@section('content')
<div class="container">
    <div><a href="/customers" class="btn btn-primary">Zákazníci</a></div>
    <div><a href="/carriers" class="btn btn-primary mt-3">Dopravci</a></div>
    <div><a href="/vehicles" class="btn btn-primary mt-3">Dopravní prostředky</a></div>
    <div><a href="/sizes" class="btn btn-primary mt-3">Typy a velikosti</a></div>
</div>
@endsection
