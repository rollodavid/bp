@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Zákazníci</h3>
    <table class="table mt-4" style="text-align: center; margin: 0 auto; width: 100%;">
        <thead>
            <th>Název</th>
            <th>Sídlo</th>
            <th>IČO</th>
            <th>DIČ</th>
            <th>Kontaktní osoba</th>
            <th>Email</th>
            <th>Telefon</th>
            <th><a href="/customers/create" class="btn btn-primary"><b>+</b></a></th>
        </thead>
        <tbody>
            @foreach ($customers as $customer)
                <tr>
                    <td>{{ $customer->name }}</td>
                    <td>{{ Str::limit($customer->address->show(), 30) }}</td>
                    <td>{{ $customer->ico }}</td>
                    <td>{{ $customer->dic }}</td>
                    <td>{{ $customer->contacts[0]->contactname ?? null }}</td>
                    <td>{{ $customer->contacts[0]->email ?? null }}</td>
                    <td>{{ $customer->contacts[0]->tel ?? null }}</td>
                    <td><a href="/customers/{{ $customer->id }}/edit" class="btn btn-primary">Upravit</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
