@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Editace zákazníka</h3>
    <form action="/customers/{{ $customer->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PATCH')
        
        <div class="col-10 offset-1">
            <!-- Fakturační údaje -->
            <div class="row mb-5 mt-5">
                <legend>Základní informace</legend>
                <div class="col-4">
                    <label for="name" class="col-form-label">Název zákazníka</label>
                    <input id="name"
                        type="text"
                        class="form-control @error('name') is-invalid @enderror"
                        name="name" value="{{ old('name') ?? $customer->name }}"
                        autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="ico" class="col-form-label">IČO</label>
                    <input id="ico"
                        type="text"
                        class="form-control @error('ico') is-invalid @enderror"
                        name="ico" value="{{ old('ico') ?? $customer->ico }}"
                        autocomplete="ico" autofocus required>
                    @error('ico')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="dic" class="col-form-label">DIČ</label>
                    <input id="dic"
                        type="text"
                        class="form-control @error('dic') is-invalid @enderror"
                        name="dic" value="{{ old('dic') ?? $customer->dic }}"
                        autocomplete="dic" autofocus required>
                    @error('dic')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Adresa -->
            <div class="row mb-5 mt-5">
                <legend>Sídlo</legend>
                <div class="col-4">
                    <label for="city" class="col-form-label">Město</label>
                    <input id="city"
                        type="text"
                        class="form-control @error('city') is-invalid @enderror"
                        name="city" value="{{ old('city') ?? $customer->address->city }}"
                        autocomplete="city" autofocus required>
                    @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="street" class="col-form-label">Ulice</label>
                    <input id="street"
                        type="text"
                        class="form-control @error('street') is-invalid @enderror"
                        name="street" value="{{ old('street') ?? $customer->address->street }}"
                        autocomplete="street" autofocus required>
                    @error('street')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-2">
                    <label for="number" class="col-form-label">Číslo popisné</label>
                    <input id="number"
                        type="text"
                        class="form-control @error('number') is-invalid @enderror"
                        name="number" value="{{ old('number') ?? $customer->address->number }}"
                        autocomplete="number" autofocus required>
                    @error('number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-2">
                    <label for="psc" class="col-form-label">PSČ</label>
                    <input id="psc"
                        type="text"
                        class="form-control @error('psc') is-invalid @enderror"
                        name="psc" value="{{ old('psc') ?? $customer->address->psc }}"
                        autocomplete="psc" autofocus required>
                    @error('psc')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Kontakt -->
            <div class="row mb-5 mt-5">
                <legend>Kontakty</legend>
                <div class="col-4"><label for="contactname" class="col-form-label">Kontaktní osoba</label></div>
                <div class="col-4"><label for="email" class="col-form-label">Email</label></div>
                <div class="col-4"><label for="tel" class="col-form-label">Telefon</label></div>
                @foreach ($customer->contacts as $contact)
                    <div class="col-4 mb-2">
                        <input type="text"
                            class="form-control @error('contactname') is-invalid @enderror"
                            name="contactname[]" value="{{ old('contactname') ?? $contact->contactname }}"
                            autocomplete="contactname" autofocus required>
                        @error('contactname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-4 mb-2">
                        <input type="email"
                            class="form-control @error('email') is-invalid @enderror"
                            name="email[]" value="{{ old('email') ?? $contact->email }}"
                            autocomplete="email" autofocus required>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-4 mb-2">
                        <input type="tel" pattern="^[\+]{1}\d{3}((\d{9})|([\+]?\d{12}))$"
                            class="form-control @error('tel') is-invalid @enderror"
                            name="tel[]" value="{{ old('tel') ?? $contact->tel }}"
                            autocomplete="tel" autofocus>
                        @error('tel')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                @endforeach

                <label class="mt-2 col-form-label">Nový kontakt</label>
                <div class="col-4 mb-2">
                    <input type="text" placeholder="Kontaktní osoba"
                        class="form-control @error('contactname') is-invalid @enderror"
                        name="contactname[]" value="{{ old('contactname') }}"
                        autocomplete="contactname" autofocus>
                    @error('contactname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4 mb-2">
                    <input type="email" placeholder="Email"
                        class="form-control @error('email') is-invalid @enderror"
                        name="email[]" value="{{ old('email') }}"
                        autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-3 mb-2">
                    <input type="tel" pattern="^[\+]{1}\d{3}((\d{9})|([\+]?\d{12}))$" placeholder="Telefon"
                        class="form-control @error('tel') is-invalid @enderror"
                        name="tel[]" value="{{ old('tel') }}"
                        autocomplete="tel" autofocus>
                    @error('tel')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-1">
                    <button class="btn btn-primary w-auto">Přidat</button>
                </div>
            </div>
            <!-- Uložit -->
            <button class="btn btn-primary position-fixed" style="bottom: 24px; right: 20px;">Uložit</button>
        </div>
    </form>

    <form action="/customers/{{ $customer->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('DELETE')

        <div class="col-10 offset-1">
            <div class="row mt-2 justify-content-end gap-2">
                <button class="btn btn-danger w-auto" onclick="return confirm('Are you sure?')">Smazat</button>
                <a href="/customers" class="btn btn-secondary w-auto" onclick="return confirm('Are you sure?')">Zrušit</a>
            </div>
        </div>
    </form>
</div>
@endsection
