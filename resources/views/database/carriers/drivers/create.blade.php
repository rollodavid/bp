@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Nový řidič</h3>
    <form action="/drivers" enctype="multipart/form-data" method="POST">
        @csrf
        
        <div class="col-4 offset-4">
            <!-- Jméno -->
            <div class="row mb-3">
                <label for="name" class="col-md-4 col-form-label">Jméno řidiče</label>
                <input id="name"
                    type="text"
                    class="form-control @error('name') is-invalid @enderror"
                    name="name" value="{{ old('name') }}"
                    autocomplete="name" autofocus required>
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Dopravci -->
            <div class="row mb-3">
                <label for="carrier_id" class="col-md-4 col-form-label">Dopravce</label>
                <select name="carrier_id" id="carrier_id" class="form-select" required>
                    <option value=""></option>
                    @foreach($carriers as $carrier)
                        <option value="{{ $carrier->id }}">{{ $carrier->name }}</option>
                    @endforeach
                </select>
                @error('carrier_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Tahač -->
            <div class="row mb-3">
                <label for="vehicle_id" class="col-md-4 col-form-label">Tahač</label>
                <select name="vehicle_id" id="vehicle_id" class="form-select" required>
                    <option value=""></option>
                    @foreach($vehicles as $vehicle)
                        <option value="{{$vehicle->id}}">{{$vehicle->rz}} ({{$vehicle->height->name}})</option>
                    @endforeach
                </select>
                @error('vehicle_id')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Kontakt -->
            <div class="row mb-3">
                <label for="tel" class="col-md-4 col-form-label">Telefon</label>
                <input type="tel" pattern="^[\+]{1}\d{3}((\d{9})|([\+]?\d{12}))$"
                    class="form-control @error('tel') is-invalid @enderror"
                    name="tel" value="{{ old('tel') }}"
                    autocomplete="tel" autofocus>
                @error('tel')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <!-- Přidat -->
            <div class="row mt-4 justify-content-end">
                <button class="btn btn-primary w-auto">Přidat</button>
            </div>
        </div>
    </form>
</div>
@endsection
