@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Editace dopravce</h3>
    <form action="/carriers/{{ $carrier->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PATCH')
        
        <div class="col-10 offset-1">
            <!-- Fakturační údaje -->
            <div class="row mb-5 mt-5">
                <legend>Základní informace</legend>
                <div class="col-4">
                    <label for="name" class="col-form-label">Název dopravce</label>
                    <input id="name"
                        type="text"
                        class="form-control @error('name') is-invalid @enderror"
                        name="name" value="{{ old('name') ?? $carrier->name }}"
                        autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="ico" class="col-form-label">IČO</label>
                    <input id="ico"
                        type="text"
                        class="form-control @error('ico') is-invalid @enderror"
                        name="ico" value="{{ old('ico') ?? $carrier->ico }}"
                        required
                        autocomplete="ico" autofocus>
                    @error('ico')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="dic" class="col-form-label">DIČ</label>
                    <input id="dic"
                        type="text"
                        class="form-control @error('dic') is-invalid @enderror"
                        name="dic" value="{{ old('dic') ?? $carrier->dic }}"
                        required
                        autocomplete="dic" autofocus>
                    @error('dic')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Adresa -->
            <div class="row mb-5 mt-5">
                <legend>Sídlo</legend>
                <div class="col-4">
                    <label for="city" class="col-form-label">Město</label>
                    <input id="city"
                        type="text"
                        class="form-control @error('city') is-invalid @enderror"
                        name="city" value="{{ old('city') ?? $carrier->address->city }}"
                        required
                        autocomplete="city" autofocus>
                    @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="street" class="col-form-label">Ulice</label>
                    <input id="street"
                        type="text"
                        class="form-control @error('street') is-invalid @enderror"
                        name="street" value="{{ old('street') ?? $carrier->address->street }}"
                        autocomplete="street" autofocus>
                    @error('street')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-2">
                    <label for="number" class="col-form-label">Číslo popisné</label>
                    <input id="number"
                        type="text"
                        class="form-control @error('number') is-invalid @enderror"
                        name="number" value="{{ old('number') ?? $carrier->address->number }}"
                        required
                        autocomplete="number" autofocus>
                    @error('number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-2">
                    <label for="psc" class="col-form-label">PSČ</label>
                    <input id="psc"
                        type="text"
                        class="form-control @error('psc') is-invalid @enderror"
                        name="psc" value="{{ old('psc') ?? $carrier->address->psc }}"
                        required
                        autocomplete="psc" autofocus>
                    @error('psc')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Kontakt -->
            <div class="row mb-5 mt-5">
                <legend>Kontakt</legend>
                <div class="col-4"><label for="contactname" class="col-form-label">Kontaktní osoba</label></div>
                <div class="col-4"><label for="email" class="col-form-label">Email</label></div>
                <div class="col-4"><label for="tel" class="col-form-label">Telefon</label></div>
                @foreach ($carrier->contacts as $contact)
                    <div class="col-4 mb-2">
                        <input type="text"
                            class="form-control @error('contactname') is-invalid @enderror"
                            name="contactname[]" value="{{ old('contactname') ?? $contact->contactname }}"
                            autocomplete="contactname" autofocus required>
                        @error('contactname')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-4 mb-2">
                        <input type="email"
                            class="form-control @error('email') is-invalid @enderror"
                            name="email[]" value="{{ old('email') ?? $contact->email }}"
                            autocomplete="email" autofocus required>
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-4 mb-2">
                        <input type="tel" pattern="^[\+]{1}\d{3}((\d{9})|([\+]?\d{12}))$"
                            class="form-control @error('tel') is-invalid @enderror"
                            name="tel[]" value="{{ old('tel') ?? $contact->tel }}"
                            autocomplete="tel" autofocus>
                        @error('tel')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                @endforeach

                <label class="mt-2 col-form-label">Nový kontakt</label>
                <div class="col-4 mb-2">
                    <input type="text" placeholder="Kontaktní osoba"
                        class="form-control @error('contactname') is-invalid @enderror"
                        name="contactname[]" value="{{ old('contactname') }}"
                        autocomplete="contactname" autofocus>
                    @error('contactname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4 mb-2">
                    <input type="email" placeholder="Email"
                        class="form-control @error('email') is-invalid @enderror"
                        name="email[]" value="{{ old('email') }}"
                        autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-3 mb-2">
                    <input type="tel" pattern="^[\+]{1}\d{3}((\d{9})|([\+]?\d{12}))$" placeholder="Telefon"
                        class="form-control @error('tel') is-invalid @enderror"
                        name="tel[]" value="{{ old('tel') }}"
                        autocomplete="tel" autofocus>
                    @error('tel')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-1">
                    <button class="btn btn-primary w-auto">Přidat</button>
                </div>
            </div>
            <!-- Řidiči -->
            <div class="row mb-5 mt-5">
                <legend>Řidiči</legend>
                <table class="table text-center">
                    <thead>
                        <th>Jméno</th>
                        <th>Telefon</th>
                        <th>RZ tahače</th>
                        <th>Výškový profil</th>
                        <th><a href="/drivers/create" class="btn btn-primary"><b>+</b></a></th>
                    </thead>
                    <tbody>
                        @foreach ($drivers as $driver)
                            <tr>
                                <td>{{ $driver->name }}</td>
                                <td>{{ $driver->tel }}</td>
                                <td>{{ $driver->vehicle->rz }}</td>
                                <td>{{ $driver->vehicle->height->name }}</td>
                                <td><a href="/drivers/{{ $driver->id }}/edit" class="btn btn-primary">Upravit</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- Chassises -->
            <div class="row mb-5 mt-5">
                <legend>Šasi</legend>
                <table class="table text-center">
                    <thead>
                        <th>RZ</th>
                        <th>Výškový profil</th>
                        <th><a href="/vehicles/create" class="btn btn-primary"><b>+</b></a></th>
                    </thead>
                    <tbody>
                        @foreach ($chassises as $chassis)
                            <tr>
                                <td>{{ $chassis->rz }}</td>
                                <td>{{ $chassis->height->name }}</td>
                                <td><a href="/vehicles/{{ $chassis->id }}/edit" class="btn btn-primary">Upravit</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- Uložit -->
            <button class="btn btn-primary position-fixed" style="bottom: 24px; right: 20px;">Uložit</button>
        </div>
    </form>

    <form action="/carriers/{{ $carrier->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('DELETE')

        <div class="col-10 offset-1">
            <div class="row mt-3 justify-content-end gap-2">
                <button class="btn btn-danger w-auto" onclick="return confirm('Are you sure?')">Smazat</button>
                <a href="/carriers" class="btn btn-secondary w-auto" onclick="return confirm('Are you sure?')">Zrušit</a>
            </div>
        </div>
    </form>
</div>
@endsection
