@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Dopravci</h3>
    <table class="table mt-4 w-100" style="text-align: center; margin: 0 auto;">
        <thead>
            <th>Název</th>
            <th>Sídlo</th>
            <th>IČO</th>
            <th>DIČ</th>
            <th>Kontaktní osoba</th>
            <th>Email</th>
            <th>Telefon</th>
            <th>Řidiči</th>
            <th>Šasi</th>
            <th><a href="/carriers/create" class="btn btn-primary"><b>+</b></a></th>
        </thead>
        <tbody>
            @foreach ($carriers as $key => $carrier)
                <tr>
                    <td>{{ $carrier->name }}</td>
                    <td>{{ Str::limit($carrier->address->show(), 30) }}</td>
                    <td>{{ $carrier->ico }}</td>
                    <td>{{ $carrier->dic }}</td>
                    <td>{{ $carrier->contacts[0]->contactname ?? null }}</td>
                    <td>{{ $carrier->contacts[0]->email ?? null }}</td>
                    <td>{{ $carrier->contacts[0]->tel ?? null }}</td>
                    <td>{{ $carrier->drivers->count() }}</td>
                    <td>{{ $chassises[$key] }}</td>
                    <td><a href="/carriers/{{ $carrier->id }}/edit" class="btn btn-primary">Upravit</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
