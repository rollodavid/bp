@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Nový dopravce</h3>
    <form action="/carriers" enctype="multipart/form-data" method="POST">
        @csrf
        
        <div class="col-8 offset-2">
            <!-- Fakturační údaje -->
            <div class="row mb-3">
                <legend>Základní informace</legend>
                <div class="col-6">
                    <label for="name" class="col-form-label">Název dopravce</label>
                    <input id="name"
                        type="text"
                        class="form-control @error('name') is-invalid @enderror"
                        name="name" value="{{ old('name') }}"
                        autocomplete="name" autofocus>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-3">
                    <label for="ico" class="col-form-label">IČO</label>
                    <input id="ico"
                        type="text"
                        class="form-control @error('ico') is-invalid @enderror"
                        name="ico" value="{{ old('ico') }}"
                        autocomplete="ico" autofocus>
                    @error('ico')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-3">
                    <label for="dic" class="col-form-label">DIČ</label>
                    <input id="dic"
                        type="text"
                        class="form-control @error('dic') is-invalid @enderror"
                        name="dic" value="{{ old('dic') }}"
                        autocomplete="dic" autofocus>
                    @error('dic')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Adresa -->
            <div class="row mb-3">
                <legend>Sídlo</legend>
                <div class="col-5">
                    <label for="city" class="col-form-label">Město</label>
                    <input id="city"
                        type="text"
                        class="form-control @error('city') is-invalid @enderror"
                        name="city" value="{{ old('city') }}"
                        autocomplete="city" autofocus>
                    @error('city')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-3">
                    <label for="street" class="col-form-label">Ulice</label>
                    <input id="street"
                        type="text"
                        class="form-control @error('street') is-invalid @enderror"
                        name="street" value="{{ old('street') }}"
                        autocomplete="street" autofocus>
                    @error('street')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-2">
                    <label for="number" class="col-form-label">Číslo popisné</label>
                    <input id="number"
                        type="text"
                        class="form-control @error('number') is-invalid @enderror"
                        name="number" value="{{ old('number') }}"
                        autocomplete="number" autofocus>
                    @error('number')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-2">
                    <label for="psc" class="col-form-label">PSČ</label>
                    <input id="psc"
                        type="text"
                        class="form-control @error('psc') is-invalid @enderror"
                        name="psc" value="{{ old('psc') }}"
                        autocomplete="psc" autofocus>
                    @error('psc')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Kontakt -->
            <div class="row mb-3">
                <legend>Kontakt</legend>
                <div class="col-4">
                    <label for="contactname" class="col-form-label">Kontaktní osoba</label>
                    <input type="text"
                        class="form-control @error('contactname') is-invalid @enderror"
                        name="contactname" value="{{ old('contactname') }}"
                        autocomplete="contactname" autofocus>
                    @error('contactname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="email" class="col-form-label">Email</label>
                    <input type="email"
                        class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}"
                        autocomplete="email" autofocus>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="tel" class="col-form-label">Telefon</label>
                    <input type="tel" pattern="^[\+]{1}\d{3}((\d{9})|([\+]?\d{12}))$"
                        class="form-control @error('tel') is-invalid @enderror"
                        name="tel" value="{{ old('tel') }}"
                        autocomplete="tel" autofocus>
                    @error('tel')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Přidat -->
            <div class="row mt-4 justify-content-end">
                <button class="btn btn-primary w-auto">Přidat</button>
            </div>
        </div>
    </form>
</div>
@endsection
