@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Zaslání nacenění zákazníkovi</h3>
    <form action="/pricing" enctype="multipart/form-data" method="post">
        @csrf

        <input type="hidden" name="demand_id" id="demand_id" value="{{$demand->id}}">
        <input type="hidden" name="pricing_id" id="pricing_id" value="{{$pricing->id}}">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="row mb-3 mt-3">
                    <legend>Adresát</legend>
                    <div class="col-6 mb-3">
                        <label for="to">Email</label>
                        <input type="text" name="to" id="to" class="form-control" value="{{$demand->customer->contacts[0]->email}}">
                    </div>
                    <div class="col-6 mb-3">
                        <label for="to">Zákazník</label>
                        <input type="text" name="name" id="name" class="form-control" value="{{$demand->customer->name}}" readonly>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="cc">Kopie</label>
                        <input type="text" name="cc" id="cc" class="form-control" value="
@for($i=1;$i<count($demand->customer->contacts);$i++)
@if($i+1 < count($demand->customer->contacts)){{$demand->customer->contacts[$i]->email}}, 
@else{{$demand->customer->contacts[$i]->email}}@endif
@endfor">
                    </div>
                </div>
                <div class="row mb-3">
                    <legend>Obsah</legend>
                    <div class="col-12 mb-3">
                        <label for="subject">Předmět</label>
                        <input type="text" name="subject" id="subject" class="form-control" value="Nacenění přepravy">
                    </div>
                    <div class="col-12 mb-3">
                        <label for="pricing">Nacenění</label>
                        <textarea name="pricing" id="pricing" rows="4" class="form-control" readonly>Cena přepravy: {{$pricing->carrier_total}} euro&#13;&#10;Čekání v ceně: {{$pricing->free}} hodin&#13;&#10;Každá další započatá hodina: {{$pricing->hour}} euro&#13;&#10;Cena za celní služby: {{$demand->service_price}} euro</textarea>
                    </div>
                    <div class="col-12 mb-3">
                        <label for="content">Přeprava</label>
                        <textarea name="content" id="content" rows="8" class="form-control" readonly>{{$demand->type->name}} ({{$demand->size->value}})&#13;&#10;&#13;&#10;@foreach ($demand->locations as $location){{$location->type->name}}: {{$location->address->city}}, {{$location->address->street}} {{$location->address->number}}, {{$location->address->psc}}@if ($location->type->name == 'Vyzvednutí') | Netto {{$demand->netto_pick}} Kg @endif @if ($location->type->name == 'Nakládka') | Netto {{$demand->netto_load}} Kg @endif&#13;&#10;@endforeach</textarea>
                    </div>
                </div>
                <!-- Note -->
                <div class="row mb-3">
                    <legend>Poznámka</legend>
                    <div class="col-12">
                        <textarea name="note" id="note" rows="3" class="form-control" placeholder="..."></textarea>
                    </div>
                </div>
                <!-- Uložit -->
                <div class="row mt-4 d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary w-auto">Odeslat</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection