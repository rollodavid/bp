@component('mail::message')
<p>Dobrý den,</p>
<p>zasílám nacenění přepravy:</p>

<ul>
<li>Cena přepravy: {{$pricing->carrier_total}} euro</li>
<li>Čekání v ceně: {{$pricing->free}} hodin</li>
<li>Každá další započatá hodina: {{$pricing->hour}} euro</li>
<li>Cena za celní služby: {{$demand->service_price}} euro</li>
</ul>

<p>Přeprava:</p>
<ul>
<li>{{$demand->type->name}} ({{$demand->size->value}})</li>
@foreach ($demand->locations as $location)
<li>{{$location->type->name}}: {{$location->address->city}}, {{$location->address->street}} {{$location->address->number}}, {{$location->address->psc}}@if ($location->type->name == 'Vyzvednutí') | Netto {{$demand->netto_pick}} Kg @endif @if ($location->type->name == 'Nakládka') | Netto {{$demand->netto_load}} Kg @endif</li>
@endforeach
</ul>

@if (!empty($note))
<p>{{$note}}</p>
@endif

<p>Děkuji.</p>

<p style="margin: 0">S pozdravem</p>
<p style="margin: 0">{{$username}}</p>
@endcomponent