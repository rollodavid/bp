@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Zaslání nabídky dopravcům</h3>
    <form action="/demand-info" enctype="multipart/form-data" method="post">
        @csrf

        <input type="hidden" name="demand_id" id="demand_id" value="{{$demand->id}}">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="row mb-3 mt-3">
                    <label for="to">Adresát</label>
                    <input type="text" name="to" id="to" class="form-control"
                        value="
@for($i=0;$i<count($demand->prices);$i++)
@if($i+1 < count($demand->prices)){{$demand->prices[$i]->carrier->contacts[0]->email}}, 
@else{{$demand->prices[$i]->carrier->contacts[0]->email}}@endif
@endfor">
                </div>
        
                <div class="row mb-3">
                    <label for="subject">Předmět</label>
                    <input type="text" name="subject" id="subject" class="form-control" value="Nacenění přepravy">
                </div>

                <div class="row mb-3">
                    <label for="content">Přeprava</label>
                    <textarea name="content" id="content" rows="8" class="form-control" readonly>{{$demand->type->name}} ({{$demand->size->value}})&#13;&#10;&#13;&#10;@foreach ($demand->locations as $location){{$location->type->name}}: {{$location->address->city}}, {{$location->address->street}} {{$location->address->number}}, {{$location->address->psc}}@if ($location->type->name == 'Vyzvednutí') | Netto {{$demand->netto_pick}} Kg @endif @if ($location->type->name == 'Nakládka') | Netto {{$demand->netto_load}} Kg @endif&#13;&#10;@endforeach</textarea>
                </div>

                <div class="row mb-2">
                    <label for="note">Poznámka</label>
                    <textarea name="note" id="note" rows="3" class="form-control" placeholder="..."></textarea>
                </div>

                <!-- Uložit -->
                <div class="row mt-4 d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary w-auto">Odeslat</button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection