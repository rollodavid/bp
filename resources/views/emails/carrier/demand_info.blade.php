@component('mail::message')
<p>Dobrý den,</p>
<p>prosím o nacenění přepravy:</p>

<ul>
<li>{{$demand->type->name}} ({{$demand->size->value}})</li>
@foreach ($demand->locations as $location)
<li>{{$location->type->name}}: {{$location->address->city}}, {{$location->address->street}} {{$location->address->number}}, {{$location->address->psc}}@if ($location->type->name == 'Vyzvednutí') | Netto {{$demand->netto_pick}} Kg @endif @if ($location->type->name == 'Nakládka') | Netto {{$demand->netto_load}} Kg @endif</li>
@endforeach
</ul>

@if (!empty($note))
<p>{{$note}}</p>
@endif

<p>Děkuji.</p>

<p style="margin: 0">S pozdravem</p>
<p style="margin: 0">{{$username}}</p>
@endcomponent