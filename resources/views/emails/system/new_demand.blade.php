@component('mail::message')
<p>Nová poptávka od {{$demand->name}}</p>
<p>Přeprava</p>
<ul>
@foreach ($demand->locations as $location)
<li>{{$location->type->name}}: {{$location->address->city}}, {{$location->address->street}} {{$location->address->number}}, {{$location->address->psc}}</li>
@endforeach
</ul>
<p>Poznámka od zákazníka:</p>
<p>{{$demand->note}}</p>
@endcomponent