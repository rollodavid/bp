@extends('layouts.app')

@section('content')
<div class="container">
    <table class="table w-auto mt-3" style="text-align: center;">
        <thead>
            <th></th>
            <th>Počet</th>
        </thead>
        <tbody>
            <tr>
                <td>Uživatelé</td>
                <td>{{count($users)}}</td>
            </tr>
            <tr>
                <td>Přepravy</td>
                <td>{{count($transports)}}</td>
            </tr>
            <tr>
                <td>Poptávky</td>
                <td>{{count($demands)}}</td>
            </tr>
            <tr>
                <td>Zákazníci</td>
                <td>{{count($customers)}}</td>
            </tr>
            <tr>
                <td>Dopravci</td>
                <td>{{count($carriers)}}</td>
            </tr>
            <tr>
                <td>Řidiči</td>
                <td>{{count($drivers)}}</td>
            </tr>
            <tr>
                <td>Tahače</td>
                <td>{{count($trucks)}}</td>
            </tr>
            <tr>
                <td>Šasi</td>
                <td>{{count($chassises)}}</td>
            </tr>
            <tr>
                <td>Adresy</td>
                <td>{{count($sizes)}}</td>
            </tr>
            <tr>
                <td>Typy PJ</td>
                <td>{{count($types)}}</td>
            </tr>
            <tr>
                <td>Velikosti PJ</td>
                <td>{{count($sizes)}}</td>
            </tr>
        </tbody>
    </table>
</div>
@endsection