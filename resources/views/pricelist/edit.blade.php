@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Detail poptávky</h3>
    <form action="/demands/{{ $demand->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PATCH')
        
        <div class="col-10 offset-1">
            <!-- Zákazník -->
            <div class="row mb-5 mt-5">
                <legend>Zákazník</legend>
                <div class="col-4">
                    <label for="name" class="col-form-label">Název</label>
                    <input id="name"
                        type="text"
                        class="form-control @error('name') is-invalid @enderror"
                        name="name" value="{{ old('name') ?? $demand->name }}"
                        autocomplete="name" autofocus required>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="email" class="col-form-label">Kontaktní email</label>
                    <input id="email"
                        type="text"
                        class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') ?? $demand->email }}"
                        autocomplete="email" autofocus required>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="customer_id" class="col-form-label">Zákazník</label>
                    <select name="customer_id" id="customer_id" class="form-select" required>
                        <option value=""></option>
                        @foreach($customers as $customer)
                            <option value="{{ $customer->id }}" @if($customer->id==($demand->customer->id ?? null)) selected='selected' @endif>{{ $customer->name }}</option>
                        @endforeach
                    </select>
                    @error('customer_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- PJ -->
            <div class="row mb-5 mt-5">
                <legend>Přepravní jednotka</legend>
                <div class="col-6">
                    <label for="type_id" class="col-form-label">Typ jednotky</label>
                    <select name="type_id" id="type_id" class="form-select" required>
                        <option value=""></option>
                        @foreach($types as $type)
                            <option value="{{ $type->id }}" @if($type->id==($demand->type->id ?? null)) selected='selected' @endif>{{ $type->name }}</option>
                        @endforeach
                    </select>
                    @error('type_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="size_id" class="col-form-label">Velikost jednotky</label>
                    <select name="size_id" id="size_id" class="form-select" required>
                        <option value=""></option>
                        @foreach($sizes as $size)
                            <option value="{{ $size->id }}" @if($size->id==($demand->size->id ?? null)) selected='selected' @endif>{{ $size->value }}</option>
                        @endforeach
                    </select>
                    @error('size_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="netto_pick" class="col-form-label w-100">Váha zboží (netto) při vyzvednutí v Kg</label>
                    <input id="netto_pick"
                        type="number"
                        class="form-control @error('netto_pick') is-invalid @enderror"
                        name="netto_pick" value="{{ old('netto_pick') ?? $demand->netto_pick }}"
                        autocomplete="netto_pick" autofocus required>
                    @error('netto_pick')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="netto_load" class="col-form-label w-100">Váha zboží (netto) při nakládce v Kg</label>
                    <input id="netto_load"
                        type="number"
                        class="form-control @error('netto_load') is-invalid @enderror"
                        name="netto_load" value="{{ old('netto_load') ?? $demand->netto_load }}"
                        autocomplete="netto_load" autofocus>
                    @error('netto_load')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Dopravci -->
            <div class="row mb-5 mt-5">
                <div class="d-flex align-items-center justify-content-between">
                    <legend class="w-auto">Dopravci</legend>
                    @if (count($demand->prices) > 0)
                        <a href="/demand-info/{{$demand->id}}" class="btn btn-warning">Poptat dopravce</a>
                    @endif
                </div>
                <div class="container d-flex gap-2">
                    @if (count($demand->prices) > 0)
                        <div class="mb-3 col-2 d-flex gap-1 flex-column">
                            <input type="text" class="form-control text-right" style="border-color: transparent;" value="Dopravce">
                            <input type="text" class="form-control text-right" style="border-color: transparent;" value="Cena dopravce">
                            <input type="text" class="form-control text-right" style="border-color: transparent;" value="Čekání v ceně">
                            <input type="text" class="form-control text-right" style="border-color: transparent;" value="Čekání/h">
                            <input type="text" class="form-control text-right" style="border-color: transparent;" value="Procenta">
                            <input type="text" class="form-control text-right" style="border-color: transparent;" value="Celkem">
                        </div>
                    @endif
                    @foreach ($prices as $price)
                        <div class="mb-3 d-flex gap-1 flex-column text-center">
                            <!-- Select -->
                            <input type="hidden" class="form-control" name="carrier_id[]" value="{{$price->carrier->id}}" readonly>
                            <input type="text" class="form-control" name="carrier_name[]" value="{{Str::limit($price->carrier->name, 20)}}" readonly>
                            <!-- Carrier price -->
                            <input type="number" class="form-control carrier_price" name="carrier_price[]" onchange="culTotal()" placeholder="Cena dopravce" value={{$price->carrier_price}}>
                            <!-- Waiting free hours -->
                            <input type="number" class="form-control" name="free[]" placeholder="Čekání v ceně" value={{$price->free ?? 2}}>
                            <!-- Waiting price per hour -->
                            <input type="number" class="form-control" name="hour[]" placeholder="Cena čekání/h" value={{$price->hour ?? 20}}>
                            <!-- Percentage -->
                            <input type="number" step="any" class="form-control percentage" name="percentage[]" onchange="culTotal()" placeholder="Procenta" value={{$price->percentage ?? 5}}>
                            <!-- Carrier total -->
                            <input type="number" class="form-control carrier_total" name="carrier_total[]" onchange="culPer()" placeholder="Celkem" value={{$price->carrier_total}}>
                            <!-- Zaslat cenu -->
                            <a href="/pricing/{{$demand->id}}/{{$price->id}}" class="btn btn-success">Zaslat cenu zákazníkovi</a>
                            <!-- Realizovat -->
                            <a href="/realize/{{$demand->id}}/{{$price->carrier->id}}" class="btn btn-primary">Realizovat přepravu</a>
                        </div>
                    @endforeach
                    @livewire('demand-carrier')
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
                    @livewireScripts
                </div>
            </div>
            <!-- Celní služby -->
            <div class="row mb-5 mt-5">
                <legend>Celní služby</legend>
                <div class="form-check ms-4">
                    <label for="service_new" class="form-check-label">Vystavení nové T1</label>
                    <input id="service_new"
                        type="checkbox"
                        value="new"
                        class="form-check-input @error('service_new') is-invalid @enderror"
                        name="service_new" value="{{ old('service_new') }}"
                        autocomplete="service_new" autofocus
                        {{ $demand->service_new ? 'checked' : '' }}>
                    @error('service_new')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-check ms-4">
                    <label for="service_end" class="form-check-label">Ukončení T1</label>
                    <input id="service_end"
                        type="checkbox"
                        value="end"
                        class="form-check-input @error('service_end') is-invalid @enderror"
                        name="service_end" value="{{ old('service_end') }}"
                        autocomplete="service_end" autofocus
                        {{ $demand->service_end ? 'checked' : '' }}>
                    @error('service_end')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-check ms-4">
                    <label for="service_free" class="form-check-label">Propuštění do volného oběhu</label>
                    <input id="service_free"
                        type="checkbox"
                        value="free"
                        class="form-check-input @error('service_free') is-invalid @enderror"
                        name="service_free" value="{{ old('service_free') }}"
                        autocomplete="service_free" autofocus
                        {{ $demand->service_free ? 'checked' : '' }}>
                    @error('service_free')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-2">
                    <label for="service_price" class="col-form-label">Cena za celní služby</label>
                    <input id="service_price"
                        type="number"
                        class="form-control @error('service_price') is-invalid @enderror"
                        name="service_price" value="{{ old('service_price') ?? $demand->service_price }}"
                        autocomplete="service_price" autofocus required>
                    @error('service_price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Trasa -->
            <div class="row mb-5 mt-5">
                <legend>Trasa</legend>
                @foreach ($demand->locations as $key => $location)
                    <!-- Type -->
                    <div class="col-2 p-1">
                        <input type="text" class="form-control" name="type[]" value={{$location->type->name}} required>
                    </div>
                    <!-- City -->
                    <div class="col-3 p-1">
                        <input type="text" class="form-control" name="city[]" value={{$location->address->city}} required>
                    </div>
                    <!-- Street -->
                    <div class="col-3 p-1">
                        <input type="text" class="form-control" name="street[]" value={{$location->address->street}}>
                    </div>
                    <!-- Number -->
                    <div class="col-2 p-1">
                        <input type="text" class="form-control" name="number[]" value={{$location->address->number}} required>
                    </div>
                    <!-- PSČ -->
                    <div class="col-2 p-1">
                        <input type="text" class="form-control" name="psc[]" value={{$location->address->psc}} required>
                    </div>
                @endforeach
            </div>
            <!-- Celní deklarace -->
            <div class="row mb-5 mt-5">
                <legend>Kontakt na celní deklaraci</legend>
                <div class="col-4">
                    <label for="customs_contactname" class="col-form-label">Kontaktní osoba</label>
                    <input id="customs_contactname"
                        type="text"
                        class="form-control @error('customs_contactname') is-invalid @enderror"
                        name="customs_contactname" value="{{ old('customs_contactname') ?? $demand->customs_contactname }}"
                        autocomplete="customs_contactname" autofocus>
                    @error('customs_contactname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="customs_email" class="col-form-label">Email</label>
                    <input id="customs_email"
                        type="text"
                        class="form-control @error('customs_email') is-invalid @enderror"
                        name="customs_email" value="{{ old('customs_email') ?? $demand->customs_email }}"
                        autocomplete="customs_email" autofocus>
                    @error('customs_email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="customs_tel" class="col-form-label">Telefon</label>
                    <input id="customs_tel"
                        type="text"
                        class="form-control @error('customs_tel') is-invalid @enderror"
                        name="customs_tel" value="{{ old('customs_tel') ?? $demand->customs_tel }}"
                        autocomplete="customs_tel" autofocus>
                    @error('customs_tel')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Poznámka -->
            <div class="row mb-5">
                <legend class="mt-2">Poznámka</legend>
                <div class="col-12">
                    <textarea name="note" class="form-control" id="note" rows="3" placeholder="..." readonly>{{ old('note') ?? $demand->note }}</textarea>
                </div>
            </div>
            <!-- Uložit -->
            <button class="btn btn-primary position-fixed" style="bottom: 24px; right: 20px;">Uložit</button>
        </div>
    </form>

    <form action="/demands/{{ $demand->id }}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('DELETE')

        <div class="col-10 offset-1">
            <div class="row mt-2 justify-content-end gap-2">
                <button class="btn btn-danger w-auto" onclick="return confirm('Are you sure?')">Smazat</button>
                <a href="/pricelist" class="btn btn-secondary w-auto" onclick="return confirm('Are you sure?')">Zrušit</a>
            </div>
        </div>
    </form>
</div>
@endsection
