@extends('layouts.app')

@section('content')
<div class="container">
    <h3 class="mb-4 text-center">Poptávka přepravy</h3>
    <form action="/demands" enctype="multipart/form-data" method="POST">
        @csrf
        
        <div class="col-8 offset-2">
            <!-- Zákazník -->
            <div class="row mb-3">
                <legend class="mt-2">Zákazník</legend>
                <div class="col-6">
                    <label for="name" class="col-form-label w-100">Společnost</label>
                    <input id="name"
                        type="text"
                        class="form-control @error('name') is-invalid @enderror"
                        name="name" value="{{ old('name') }}"
                        autocomplete="name" autofocus required>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="email" class="col-form-label w-100">Kontaktní email</label>
                    <input id="email"   
                        type="email"
                        class="form-control @error('email') is-invalid @enderror"
                        name="email" value="{{ old('email') }}"
                        autocomplete="email" autofocus required>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- PJ -->
            <div class="row mb-3">
                <legend class="mt-2">Přepravní jednotka</legend>
                <div class="col-6">
                    <label for="type_id" class="col-form-label w-100">Typ jednotky</label>
                    <select name="type_id" id="type_id" class="form-select" required>
                        <option value=""></option>
                        @foreach($types as $type)
                            <option value="{{ $type->id }}" @if($type->id==($transport->type->id ?? null)) selected='selected' @endif>{{ $type->name }}</option>
                        @endforeach
                    </select>
                    @error('type_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="size_id" class="col-form-label w-100">Velikost jednotky</label>
                    <select name="size_id" id="size_id" class="form-select" required>
                        <option value=""></option>
                        @foreach($sizes as $size)
                            <option value="{{ $size->id }}" @if($size->id==($transport->size->id ?? null)) selected='selected' @endif>{{ $size->value }}</option>
                        @endforeach
                    </select>
                    @error('size_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="netto_pick" class="col-form-label w-100">Váha zboží (netto) při vyzvednutí v Kg</label>
                    <input id="netto_pick"
                        type="number"
                        class="form-control @error('netto_pick') is-invalid @enderror"
                        name="netto_pick" value="{{ old('netto_pick') }}"
                        autocomplete="netto_pick" autofocus required>
                    @error('netto_pick')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="netto_load" class="col-form-label w-100">Váha zboží (netto) při nakládce v Kg</label>
                    <input id="netto_load"
                        type="number"
                        class="form-control @error('netto_load') is-invalid @enderror"
                        name="netto_load" value="{{ old('netto_load') }}"
                        autocomplete="netto_load" autofocus>
                    @error('netto_load')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Celní služby -->
            <div class="row mb-3">
                <legend class="mt-2">Celní služby</legend>
                <div class="form-check ms-4">
                    <label for="service_new" class="form-check-label">Vystavení nové T1</label>
                    <input id="service_new"
                        type="checkbox"
                        value="new"
                        class="form-check-input @error('service_new') is-invalid @enderror"
                        name="service_new" value="{{ old('service_new') }}"
                        autocomplete="service_new" autofocus>
                    @error('service_new')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-check ms-4">
                    <label for="service_end" class="form-check-label">Ukončení T1</label>
                    <input id="service_end"
                        type="checkbox"
                        value="end"
                        class="form-check-input @error('service_end') is-invalid @enderror"
                        name="service_end" value="{{ old('service_end') }}"
                        autocomplete="service_end" autofocus>
                    @error('service_end')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-check ms-4">
                    <label for="service_free" class="form-check-label">Propuštění do volného oběhu</label>
                    <input id="service_free"
                        type="checkbox"
                        value="free"
                        class="form-check-input @error('service_free') is-invalid @enderror"
                        name="service_free" value="{{ old('service_free') }}"
                        autocomplete="service_free" autofocus>
                    @error('service_free')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Trasa -->
            <div class="row mb-3">
                <legend class="mt-2">Trasa</legend>
                @livewire('transport-location')
            </div>
            <!-- Clení deklarace -->
            <div class="row mb-3">
                <legend class="mt-2">Kontakt na celní deklaraci</legend>
                <div class="col-4">
                    <label for="customs_contactname" class="col-form-label w-100">Kontaktní osoba</label>
                    <input id="customs_contactname"
                        type="text"
                        class="form-control @error('customs_contactname') is-invalid @enderror"
                        name="customs_contactname" value="{{ old('customs_contactname') }}"
                        autocomplete="customs_contactname" autofocus>
                    @error('customs_contactname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="customs_email" class="col-form-label w-100">Email</label>
                    <input id="customs_email"
                        type="text"
                        class="form-control @error('customs_email') is-invalid @enderror"
                        name="customs_email" value="{{ old('customs_email') }}"
                        autocomplete="customs_email" autofocus>
                    @error('customs_email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="customs_tel" class="col-form-label w-100">Telefon</label>
                    <input id="customs_tel"
                        type="text"
                        class="form-control @error('customs_tel') is-invalid @enderror"
                        name="customs_tel" value="{{ old('customs_tel') }}"
                        autocomplete="customs_tel" autofocus>
                    @error('customs_tel')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Poznámka -->
            <div class="row mb-3">
                <legend class="mt-2">Poznámka</legend>
                <div class="col-12">
                    <textarea name="note" class="form-control" id="note" rows="3" placeholder="...">{{ old('note') }}</textarea>
                </div>
            </div>
            <!-- Odeslat -->
            <div class="row mt-5 justify-content-end">
                <button class="btn btn-primary w-auto">Odeslat</button>
            </div>
        </div>
    </form>
</div>
@endsection
		