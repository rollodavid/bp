@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="d-flex align-items-center justify-content-between">
        <h3>Ceník</h3>
        <a href="/demands/create" class="btn btn-primary">Nová poptávka</a>
    </div>

    <table class="table mt-4 text-center">
        <thead>
            <th>Zákazník</th>
            <th>Vyzvednutí</th>
            <th>Clení</th>
            <th>Vykládka</th>
            <th>Nakládka</th>
            <th>Vrácení</th>
        </thead>
        <tbody>
            @foreach ($data as $demand)
                <tr>
                    <td>{{ $demand->customer->name ?? $demand->name }}</td>

                    @foreach ($demand->locations as $location)
                        @if ($location->type->name == 'Vyzvednutí')
                            @php $pickup = $location->address->city @endphp
                        @elseif ($location->type->name == 'Clení')
                            @php $customs = $location->address->city @endphp
                        @elseif ($location->type->name == 'Vykládka')
                            @php $unload = $location->address->city @endphp
                        @elseif ($location->type->name == 'Nakládka')
                            @php $load = $location->address->city @endphp
                        @elseif ($location->type->name == 'Vrácení')
                            @php $return = $location->address->city @endphp
                        @endif
                    @endforeach

                    <td>{{ $pickup }}</td>
                    <td>{{ $customs ?? null }}</td>
                    <td>{{ $unload ?? null }}</td>
                    <td>{{ $load ?? null }}</td>
                    <td>{{ $return }}</td>
                    <td><a href="/demands/{{ $demand->id }}/edit" class="btn btn-primary">Více</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
