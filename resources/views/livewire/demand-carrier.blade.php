<div class="d-flex gap-1 flex-column">
    @foreach ($inputs as $key => $value)
        <div class="d-flex flex-row text-center">
            <!-- Carrier -->
            <div>
                <select class="form-select" wire:model="carrier_id.{{ $value }}" name="carrier_id[]" required>
                    <option value=""></option>
                    @foreach($carriers as $carrier)
                        <option value="{{$carrier->id}}">{{Str::limit($carrier->name, 20)}}</option>
                    @endforeach
                </select>
            </div>
            <!-- Carrier price -->
            <div>
                <input type="hidden" class="form-control" wire:model="carrier_price.{{ $value }}" name="carrier_price[]" placeholder="Cena dopravce" value="0" >
            </div>
            <!-- Waiting free hours -->
            <div>
                <input type="hidden" class="form-control" wire:model="free.{{ $value }}" name="free[]" placeholder="Čekání v ceně">
            </div>
            <!-- Waiting price per hour -->
            <div>
                <input type="hidden" class="form-control" wire:model="hour.{{ $value }}" name="hour[]" placeholder="Cena čekání/h">
            </div>
            <!-- Percentage -->
            <div>
                <input type="hidden" class="form-control" wire:model="percentage.{{ $value }}" name="percentage[]" placeholder="Procenta">
            </div>
            <!-- Carrier total -->
            <div>
                <input type="hidden" class="form-control" wire:model="carrier_total.{{ $value }}" name="carrier_total[]" placeholder="Celkem">
            </div>
            <!-- Remove -->
            <div class="ms-1">
                <button class="btn btn-danger" wire:click.prevent="remove({{$key}})"><b>x</b></button>
            </div>
        </div>
    @endforeach

    <!-- New -->
    <div class="col-2">
        <button type="button" class="btn btn-primary" wire:click.prevent="add({{$i}})" wire:click="fetchall" onclick="reloadFields()"><b>+</b></button>
    </div>
</div>