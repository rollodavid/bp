<div class="container">
    @foreach ($inputs as $key => $value)
        <div class="row mb-3">
            <!-- Select -->
            <div class="col-2">
                <select class="form-select" wire:model="type.{{ $value }}" name="type[]" required>
                    <option></option>
                    <option value="Vyzvednutí">Vyzvednutí</option>
                    <option value="Clení">Clení</option>
                    <option value="Vykládka">Vykládka</option>
                    <option value="Nakládka">Nakládka</option>
                    <option value="Vrácení">Vrácení</option>
                </select>
            </div>
            <!-- City -->
            <div class="col-3">
                <input type="text" class="form-control" wire:model="city.{{ $value }}" name="city[]" placeholder="City" required>
            </div>
            <!-- Street -->
            <div class="col-2">
                <input type="text" class="form-control" wire:model="street.{{ $value }}" name="street[]" placeholder="Street">
            </div>
            <!-- Number -->
            <div class="col-2">
                <input type="text" class="form-control" wire:model="number.{{ $value }}" name="number[]" placeholder="Number" required>
            </div>
            <!-- PSČ -->
            <div class="col-2">
                <input type="text" class="form-control" wire:model="psc.{{ $value }}" name="psc[]" placeholder="PSČ" required>
            </div>
            <!-- Remove -->
            <div class="col-1">
                <button class="btn btn-danger" wire:click.prevent="remove({{$key}})"><b>x</b></button>
            </div>
        </div>
    @endforeach

    <!-- New -->
    <div class="col-2">
        <button type="button" class="btn btn-primary mb-3" wire:click.prevent="add({{$i}})"><b>+</b></button>
    </div>
</div>