@extends('layouts.app')

@section('content')
<div class="container">
    <div class="d-flex justify-content-between">
        <h3>Editace přepravy</h3>
        <a href="/export/{{$transport->id}}" class="btn btn-primary">Jízdní příkaz</a>
    </div>
    <form action="/transports/{{$transport->id}}" enctype="multipart/form-data" method="POST">
        @csrf
        @method('PATCH')
        
        <div class="col-10 offset-1">
            <!-- Zákazník -->
            <div class="row mb-5 mt-5">
                <legend>Zákazník</legend>
                <div class="col-8">
                    <label for="customer" class="col-form-label">Zákazník</label>
                    <select name="customer_id" id="customer_id" class="form-select">
                        <option value=""></option>
                        @foreach($customers as $customer)
                            <option value="{{ $customer->id }}" @if($customer->id==($transport->customer->id ?? false)) selected='selected' @endif>{{ $customer->name }}</option>
                        @endforeach
                    </select>
                    @error('customer')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="date" class="col-form-label">Datum</label>
                    <input id="date"
                        type="date"
                        class="form-control @error('date') is-invalid @enderror"
                        name="date" value="{{ old('date') ?? $transport->date }}"
                        autocomplete="date" autofocus>
                    @error('date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- ILU -->
            <div class="row mb-5 mt-5">
                <legend>Přepravní jednotka</legend>
                <div class="col-4">
                    <label for="ilu" class="col-form-label">ILU</label>
                    <input id="ilu"
                        type="text"
                        class="form-control @error('ilu') is-invalid @enderror"
                        name="ilu" value="{{ old('ilu') ?? $transport->ilu }}"
                        autocomplete="ilu" autofocus>
                    @error('ilu')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="type_id" class="col-form-label">Typ PJ</label>
                    <select name="type_id" id="type_id" class="form-select">
                        <option value=""></option>
                        @foreach($types as $type)
                            <option value="{{ $type->id }}" @if($type->id==($transport->type->id ?? null)) selected='selected' @endif>{{ $type->name }}</option>
                        @endforeach
                    </select>
                    @error('type_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="size_id" class="col-form-label">Velikost</label>
                    <select name="size_id" id="size_id" class="form-select">
                        <option value=""></option>
                        @foreach($sizes as $size)
                            <option value="{{ $size->id }}" @if($size->id==($transport->size->id ?? null)) selected='selected' @endif>{{ $size->value }}</option>
                        @endforeach
                    </select>
                    @error('size_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="netto_pick" class="col-form-label w-100">Váha zboží (netto) při vyzvednutí v Kg</label>
                    <input id="netto_pick"
                        type="number"
                        class="form-control @error('netto_pick') is-invalid @enderror"
                        name="netto_pick" value="{{ old('netto_pick') ?? $transport->netto_pick }}"
                        autocomplete="netto_pick" autofocus required>
                    @error('netto_pick')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-6">
                    <label for="netto_load" class="col-form-label w-100">Váha zboží (netto) při nakládce v Kg</label>
                    <input id="netto_load"
                        type="number"
                        class="form-control @error('netto_load') is-invalid @enderror"
                        name="netto_load" value="{{ old('netto_load') ?? $transport->netto_load }}"
                        autocomplete="netto_load" autofocus>
                    @error('netto_load')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Dopravce -->
            <div class="row mb-5 mt-5">
                <legend>Dopravce</legend>
                <div class="col-4">
                    <label for="carrier_id" class="col-form-label">Dopravce</label>
                    <select name="carrier_id" id="carrier_id" class="form-select" required>
                        @foreach($carriers as $carrier)
                            <option value="{{ $carrier->id }}" @if($carrier->id==$transport->carrier->id) selected='selected' @endif>{{ $carrier->name }}</option>
                        @endforeach
                    </select>
                    @error('carrier_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-5">
                    <label for="driver_id" class="col-form-label">Řidič</label>
                    <select name="driver_id" id="driver_id" class="form-select">
                        <option value=""></option>
                        @foreach($drivers as $driver)
                            <option value="{{ $driver->id }}" @if($driver->id==($transport->driver->id ?? null)) selected='selected' @endif>{{ $driver->name }} ({{$driver->vehicle->rz}} - {{$driver->vehicle->height->name}})</option>
                        @endforeach
                    </select>
                    @error('driver_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-3">
                    <label for="chassis_id" class="col-form-label">Šasi</label>
                    <select name="chassis_id" id="chassis_id" class="form-select">
                        <option value=""></option>
                        @foreach($chassises as $chassis)
                            <option value="{{ $chassis->id }}" @if($chassis->id==($transport->chassis->id ?? null)) selected='selected' @endif>{{ $chassis->rz }} ({{$chassis->height->name}})</option>
                        @endforeach
                    </select>
                    @error('chassis_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Nacenění přepravy -->
            <div class="row mb-5 mt-5">
                <legend>Nacenění přepravy</legend>
                <div class="row">
                    <div class="col-3 offset-3">
                        <label for="carrier_price" class="col-form-label">Cena dopravce</label>
                        <input id="carrier_price"
                            type="number"
                            class="form-control carrier_price @error('carrier_price') is-invalid @enderror"
                            name="carrier_price" value="{{ old('carrier_price') ?? $transport->carrier_price }}"
                            autocomplete="carrier_price" autofocus readonly>
                        @error('carrier_price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <label for="percentage" class="col-form-label">Procenta</label>
                        <input id="percentage"
                            type="number" step="any"
                            class="form-control percentage @error('percentage') is-invalid @enderror"
                            name="percentage" value="{{ old('percentage') ?? $transport->percentage }}"
                            autocomplete="percentage" autofocus readonly>
                        @error('percentage')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <label for="carrier_total" class="col-form-label">Celkem za dopravce</label>
                        <input id="carrier_total"
                            type="number"
                            class="form-control carrier_total transport_carrier_total @error('carrier_total') is-invalid @enderror"
                            name="carrier_total" value="{{ old('carrier_total') ?? $transport->carrier_total }}"
                            autocomplete="carrier_total" autofocus readonly>
                        @error('carrier_total')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <label for="free_hours" class="col-form-label">Čekání v ceně</label>
                        <input id="free_hours"
                            type="number"
                            class="form-control free_hours @error('free_hours') is-invalid @enderror"
                            name="free_hours" value="{{ old('free_hours') ?? $transport->free_hours }}"
                            autocomplete="free_hours" autofocus readonly>
                        @error('free_hours')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <label for="waiting_price_by_hour" class="col-form-label">Čekání/h</label>
                        <input id="waiting_price_by_hour"
                            type="number"
                            class="form-control waiting_price_by_hour @error('waiting_price_by_hour') is-invalid @enderror"
                            name="waiting_price_by_hour" value="{{ old('waiting_price_by_hour') ?? $transport->waiting_price_by_hour }}"
                            autocomplete="waiting_price_by_hour" autofocus readonly>
                        @error('waiting_price_by_hour')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <label for="waiting_hours" class="col-form-label">Reálné čekání</label>
                        <input id="waiting_hours"
                            type="number" step="any"
                            class="form-control transport_waiting @error('waiting_hours') is-invalid @enderror"
                            name="waiting_hours" value="{{ old('waiting_hours') ?? $transport->waiting_hours }}"
                            autocomplete="waiting_hours" autofocus onchange="culWaitingPrice(),culTransportTotal()">
                        @error('waiting_hours')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-3">
                        <label for="waiting" class="col-form-label">Cena za čekání</label>
                        <input id="waiting"
                            type="number"
                            class="form-control transport_waiting_price @error('waiting') is-invalid @enderror"
                            name="waiting" value="{{ old('waiting') ?? $transport->waiting }}"
                            autocomplete="waiting" autofocus onchange="culTransportTotal()">
                        @error('waiting')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 offset-9">
                        <label for="service_price" class="col-form-label">Celní služby</label>
                        <input id="service_price"
                            type="number"
                            class="form-control transport_service @error('service_price') is-invalid @enderror"
                            name="service_price" value="{{ old('service_price') ?? $transport->service_price }}"
                            autocomplete="service_price" autofocus readonly>
                        @error('service_price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col-3 offset-9">
                        <label for="total" class="col-form-label">Celkem</label>
                        <input id="total"
                            type="number"
                            class="form-control transport_total @error('total') is-invalid @enderror"
                            name="total" value="{{ old('total') ?? $transport->total }}"
                            autocomplete="total" autofocus readonly>
                        @error('total')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>
            <!-- Celní služby (checkboxy) -->
            <div class="row mb-5 mt-5">
                <legend>Celní služby</legend>
                <div class="form-check ms-4">
                    <label for="service_new" class="form-check-label">Vystavení nové T1</label>
                    <input id="service_new"
                        type="checkbox"
                        value="new"
                        class="form-check-input @error('service_new') is-invalid @enderror"
                        name="service_new" value="{{ old('service_new') }}"
                        autocomplete="service_new" autofocus
                        {{ $transport->service_new ? 'checked' : '' }}>
                    @error('service_new')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-check ms-4">
                    <label for="service_end" class="form-check-label">Ukončení T1</label>
                    <input id="service_end"
                        type="checkbox"
                        value="end"
                        class="form-check-input @error('service_end') is-invalid @enderror"
                        name="service_end" value="{{ old('service_end') }}"
                        autocomplete="service_end" autofocus
                        {{ $transport->service_end ? 'checked' : '' }}>
                    @error('service_end')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-check ms-4">
                    <label for="service_free" class="form-check-label">Propuštění do volného oběhu</label>
                    <input id="service_free"
                        type="checkbox"
                        value="free"
                        class="form-check-input @error('service_free') is-invalid @enderror"
                        name="service_free" value="{{ old('service_free') }}"
                        autocomplete="service_free" autofocus
                        {{ $transport->service_free ? 'checked' : '' }}>
                    @error('service_free')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Trasa -->
            <div class="row mb-5 mt-5">
                <legend>Trasa</legend>
                <div class="container d-flex gap-2 mb-4">
                    @foreach ($transport->transportLocations as $info)
                        <div class="mb-3 d-flex gap-1 flex-column text-center">
                            <!-- Type -->
                            <input type="text" class="form-control" name="type[]" value="{{$info->location->type->name}}" readonly>
                            <!-- Company -->
                            <input type="text" class="form-control" name="company[]" value="{{$info->company}}" placeholder="Company">
                            <!-- City -->
                            <input type="text" class="form-control" name="city[]" value="{{$info->location->address->city}}" readonly>
                            <!-- Street -->
                            <input type="text" class="form-control" name="street[]" value="{{$info->location->address->street}}" readonly>
                            <!-- Number -->
                            <input type="text" class="form-control" name="number[]" value="{{$info->location->address->number}}" readonly>
                            <!-- PSČ -->
                            <input type="text" class="form-control" name="psc[]" value="{{$info->location->address->psc}}" readonly>
                            <!-- Dodateční informace -->
                            <!-- Reference -->
                            @if ($info->location->type->name == 'Vyzvednutí' || $info->location->type->name == 'Vykládka' || $info->location->type->name == 'Nakládka' || $info->location->type->name == 'Vrácení')
                                <input type="text" class="form-control" name="ref[]" value="{{$info->ref}}" placeholder="Reference">
                            @endif
                            @if ($info->location->type->name == 'Clení')
                                <input type="hidden" name="ref[]">
                            @endif
                            <!-- Datum a čas od, do -->
                            @if ($info->location->type->name == 'Clení' || $info->location->type->name == 'Vykládka' || $info->location->type->name == 'Nakládka')
                                <table style="@if ($info->location->type->name == 'Clení') margin-top: 40px @endif">
                                    <tbody>
                                        <tr>
                                            <td><label class="w-100 p-2">Datum</label></td>
                                            <td><input type="date" class="form-control" name="location_date[]" value="{{$info->location_date}}"></td>
                                        </tr>
                                        <tr>
                                            <td><label class="w-100 p-2">Od</label></td>
                                            <td><input type="time" class="form-control" name="from[]" value="{{$info->from}}"></td>
                                        </tr>
                                        <tr>
                                            <td><label class="w-100 p-2">Do</label></td>
                                            <td><input type="time" class="form-control" name="to[]" value="{{$info->to}}"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            @else
                                <input type="hidden" name="location_date[]">
                                <input type="hidden" name="from[]">
                                <input type="hidden" name="to[]">
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
            <!-- Celní deklarace -->
            <div class="row mb-5 mt-5">
                <legend>Kontakt na celní deklaraci</legend>
                <div class="col-4">
                    <label for="customs_contactname" class="col-form-label">Kontaktní osoba</label>
                    <input id="customs_contactname"
                        type="text"
                        class="form-control @error('customs_contactname') is-invalid @enderror"
                        name="customs_contactname" value="{{ old('customs_contactname') ?? $transport->customs_contactname }}"
                        autocomplete="customs_contactname" autofocus>
                    @error('customs_contactname')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="customs_email" class="col-form-label">Email</label>
                    <input id="customs_email"
                        type="text"
                        class="form-control @error('customs_email') is-invalid @enderror"
                        name="customs_email" value="{{ old('customs_email') ?? $transport->customs_email }}"
                        autocomplete="customs_email" autofocus>
                    @error('customs_email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-4">
                    <label for="customs_tel" class="col-form-label">Telefon</label>
                    <input id="customs_tel"
                        type="text"
                        class="form-control @error('customs_tel') is-invalid @enderror"
                        name="customs_tel" value="{{ old('customs_tel') ?? $transport->customs_tel }}"
                        autocomplete="customs_tel" autofocus>
                    @error('customs_tel')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <!-- Poznámka -->
            <div class="row mb-5 mt-5">
                <legend>Poznámka v JP</legend>
                <div class="col-12">
                    <textarea name="travel_note" id="travel_note" rows="2" class="form-control" placeholder="max. 215 znaků" value="{{$transport->travel_note}}" maxlength="215"></textarea>
                </div>
            </div>
            <!-- Soubory -->
            <div class="row mb-5 mt-5">
                <legend>Soubory</legend>
                <div class="col-4">
                    <input id="file"
                        type="file"
                        class="form-control @error('file') is-invalid @enderror"
                        name="file"
                        autocomplete="file" autofocus>
                    @error('file')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-12">
                    <label class="col-form-label">Nahrané soubory:</label>
                    <ul>
                        @foreach ($files as $file)
                            <li><a href="/transports/files/{{$file->id}}">{{$file->name}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <!-- Uložit -->
            <button class="btn btn-primary position-fixed" style="bottom: 24px; right: 20px;">Uložit</button>
        </div>
    </form>

    <form action="/transports/{{ $transport->id }}" method="POST">
        @csrf
        @method('DELETE')

        <div class="col-10 offset-1">
            <div class="row mt-2 justify-content-end gap-2">
                <button class="btn btn-danger w-auto" onclick="return confirm('Are you sure?')">Smazat</button>
                <a href="/transports" class="btn btn-secondary w-auto" onclick="return confirm('Are you sure?')">Zrušit</a>
            </div>
        </div>
    </form>
</div>
@endsection
