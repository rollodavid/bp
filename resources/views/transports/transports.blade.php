@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Přepravy</h3>
    <table class="table mt-4 text-center">
        <thead>
            <th>ILU</th>
            <th>Zákazník</th>
            <th>Dopravce</th>
            <th>Vyzvednutí</th>
            <th>Clení</th>
            <th>Vykládka</th>
            <th>Nakládka</th>
            <th>Vrácení</th>
            <th>Cena dopravce</th>
            <th>Celní služby</th>
            <th>Čekání</th>
            <th>Cena celkem</th>
            <th>Datum</th>
        </thead>
        <tbody>
            @foreach ($transports as $transport)
                <tr>
                    <td>{{ $transport->ilu }}</td>
                    <td>{{ $transport->customer['name'] }}</td>
                    <td>{{ $transport->carrier['name'] }}</td>

                    @foreach ($transport->locations as $location)
                        @if ($location->type->name == 'Vyzvednutí')
                            @php $pickup = $location->address->city @endphp
                        @elseif ($location->type->name == 'Clení')
                            @php $customs = $location->address->city @endphp
                        @elseif ($location->type->name == 'Vykládka')
                            @php $unload = $location->address->city @endphp
                        @elseif ($location->type->name == 'Nakládka')
                            @php $load = $location->address->city @endphp
                        @elseif ($location->type->name == 'Vrácení')
                            @php $return = $location->address->city @endphp
                        @endif
                    @endforeach

                    <td>{{ $pickup }}</td>
                    <td>{{ $customs ?? null }}</td>
                    <td>{{ $unload ?? null }}</td>
                    <td>{{ $load ?? null }}</td>
                    <td>{{ $return }}</td>
                    
                    <td>{{ $transport->carrier_price }}</td>
                    <td>{{ $transport->service_price }}</td>
                    <td>{{ $transport->waiting }}</td>
                    <td>{{ $transport->total }}</td>
                    <td>
                        @if ($transport->date != null)
                            {{ date('d.m.Y', strtotime($transport->date)) }}
                        @endif
                    </td>
                    <td><a href="/transports/{{ $transport->id }}/edit" class="btn btn-primary">Upravit</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
