<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])

    <style>
        .dropdown:hover .dropdown-menu {
            display: block;
            margin-top: 0;
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand d-flex" href="{{ url('/home') }}">
                    <div>
                        <img src="https://cdduss.com/wp-content/themes/cdduss/images/logo.svg" style="height: 15px;">
                    </div>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    @if (Auth::user())
                        <ul class="navbar-nav me-auto gap-4" style="padding-left: 10px; border-left: 1px solid #ced4da">
                            <li class="nav-item"><a href="/transports" class="text-black text-decoration-none">Přepravy</a></li>
                            <li class="nav-item"><a href="/demands" class="text-black text-decoration-none">Ceník</a></li>
                            <li class="nav-item dropdown">
                                <a href="/database" id="navbarDropdown" class="text-black text-decoration-none">Databáze</a>
                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/customers">Zákazníci</a>
                                    <a class="dropdown-item" href="/carriers">Dopravci</a>
                                    <a class="dropdown-item" href="/vehicles">Dopravní prostředky</a>
                                    <a class="dropdown-item" href="/sizes">Typy a velikosti</a>
                                </div>
                            </li>
                        </ul>
                    @endif

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a href="" id="navbarDropdown" class="nav-link dropdown-toggle">{{ Auth::user()->name }}</a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    @livewireScripts
</body>

<script>
    if ('{{Session::has('alert')}}') alert('{{Session::get('alert')}}');

function culTotal() {
    var carrier_prices = document.getElementsByClassName("carrier_price");
    var percentages = document.getElementsByClassName("percentage");
    var carrier_totals = document.getElementsByClassName("carrier_total");
    for (var i = 0; i < carrier_prices.length; i++) {
        var carrier_price = parseInt(carrier_prices[i].value);
        var percentage = parseFloat(percentages[i].value);
        carrier_totals[i].value = ((carrier_price/100) * (100+percentage));
    }
}
function culPer() {
    var carrier_prices = document.getElementsByClassName("carrier_price");
    var percentages = document.getElementsByClassName("percentage");
    var carrier_totals = document.getElementsByClassName("carrier_total");
    for (var i = 0; i < carrier_prices.length; i++) {
        var carrier_price = parseInt(carrier_prices[i].value);
        var carrier_total = parseFloat(carrier_totals[i].value);
        percentages[i].value = ((carrier_total / (carrier_price/100)) - 100);
    }
}
    function culTransportTotal() {
        var transport_carrier_total = document.getElementsByClassName("transport_carrier_total")[0];
        var transport_waiting_price = document.getElementsByClassName("transport_waiting_price")[0];
        var transport_service = document.getElementsByClassName("transport_service")[0];
        var transport_total = document.getElementsByClassName("transport_total")[0];

        transport_carrier_total = transport_carrier_total.value == "" ? 0 : parseInt(transport_carrier_total.value);
        transport_waiting_price = transport_waiting_price.value == "" ? 0 : parseInt(transport_waiting_price.value);
        transport_service = transport_service.value == "" ? 0 : parseInt(transport_service.value);

        transport_total.value = (transport_carrier_total + transport_service + transport_waiting_price);
    }
    function culWaitingPrice() {
        var free_hours = document.getElementsByClassName("free_hours")[0].value;
        var waiting_price_by_hour = document.getElementsByClassName("waiting_price_by_hour")[0].value;
        
        var transport_waiting = document.getElementsByClassName("transport_waiting")[0];
        var transport_waiting_value = transport_waiting.value == "" ? 0 : parseFloat(transport_waiting.value);
        
        var result = (Math.ceil(transport_waiting_value) - free_hours) * waiting_price_by_hour;
        
        var transport_waiting_price = document.getElementsByClassName("transport_waiting_price")[0];
        transport_waiting_price.value = (result < 0 ? 0 : result);
    }
</script>

</html>
