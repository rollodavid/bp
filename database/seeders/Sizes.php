<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Size;
use App\Models\Type;

class Sizes extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Type::create([
            'name' => 'Návěs',
        ]);
        Type::create([
            'name' => 'Kontejner',
        ]);
        Type::create([
            'name' => 'Výměnná nástavba',
        ]);
        Type::create([
            'name' => 'Cisterna',
        ]);

        Size::create([
            'value' => 'Standard',
        ])->types()->sync(1);
        Size::create([
            'value' => 'Lowdeck',
        ])->types()->sync(1);
        Size::create([
            'value' => '20',
        ])->types()->sync(2);
        Size::create([
            'value' => '30',
        ])->types()->sync([2,4]);
        Size::create([
            'value' => '40',
        ])->types()->sync([2,3,4]);
        Size::create([
            'value' => '45',
        ])->types()->sync([2,3]);
    }
}
