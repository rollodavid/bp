<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Address;
use App\Models\Customer;
use App\Models\CustomerContact;

class Customers extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $id = Customer::create([
            'name' => 'Company1 s.r.o.',
            'ico' => '54367569',
            'dic' => 'CZ54367569',
            'address_id' => Address::create([
                'city' => 'City3',
                'street' => 'Steet3',
                'number' => '17',
                'psc' => '12301',
            ])->id,
        ])->id;
        CustomerContact::create([
            'customer_id' => $id,
            'contactname' => 'Květa Omáčková',
            'email' => 'test3@test.cz',
            'tel' => '+420987843234',
        ]);
        CustomerContact::create([
            'customer_id' => $id,
            'contactname' => 'Jiří Noha',
            'email' => 'test4@test.cz',
            'tel' => '+420098767675',
        ]);

        $id = Customer::create([
            'name' => 'Company2 a.s.',
            'ico' => '98765434',
            'dic' => 'CZ98765434',
            'address_id' => Address::create([
                'city' => 'City4',
                'street' => 'Street4',
                'number' => '765/4',
                'psc' => '54312',
            ])->id,
        ])->id;
        CustomerContact::create([
            'customer_id' => $id,
            'contactname' => 'Milan Těstovina',
            'email' => 'test5@test.cz',
            'tel' => '+420656454343',
        ]);
        CustomerContact::create([
            'customer_id' => $id,
            'contactname' => 'Romana Svíčková',
            'email' => 'test6@test.cz',
            'tel' => '+420989767545',
        ]);
    }
}
