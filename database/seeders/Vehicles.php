<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Carrier;
use App\Models\Driver;
use App\Models\VehicleHeight;
use App\Models\VehicleType;
use App\Models\Vehicle;

class Vehicles extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        VehicleType::create([
            'name' => 'Tahač',
        ]);
        VehicleType::create([
            'name' => 'Šasí',
        ]);

        VehicleHeight::create([
            'name' => 'Standard',
        ]);
        VehicleHeight::create([
            'name' => 'Lowdeck',
        ]);

        # Carrier1
        $id = Carrier::where('name', 'Carrier1')->first()->id;
        //Řidiči a tahače
        Driver::create([
            'name' => 'Pavlína Kováčová',
            'tel' => '+420111222331',
            'carrier_id' => $id,
            'vehicle_id' => Vehicle::create([
                'rz' => '8H69534',
                'type_id' => '1',
                'height_id' => '2',
                'carrier_id' => $id,
            ])->id,
        ]);
        Driver::create([
            'name' => 'Daniela Gašparíková',
            'tel' => '+420111222332',
            'carrier_id' => $id,
            'vehicle_id' => Vehicle::create([
                'rz' => '1P97109',
                'type_id' => '1',
                'height_id' => '2',
                'carrier_id' => $id,
            ])->id,
        ]);
        Driver::create([
            'name' => 'Jiří Švancar',
            'tel' => '+420111222333',
            'carrier_id' => $id,
            'vehicle_id' => Vehicle::create([
                'rz' => '0K84321',
                'type_id' => '1',
                'height_id' => '1',
                'carrier_id' => $id,
            ])->id,
        ]);
        //Šasí
        Vehicle::create([
            'rz' => '7A68723',
            'type_id' => '2',
            'height_id' => '1',
            'carrier_id' => $id,
        ]);
        Vehicle::create([
            'rz' => '8K63457',
            'type_id' => '2',
            'height_id' => '1',
            'carrier_id' => $id,
        ]);

        # Carrier2
        $id = Carrier::where('name', 'Carrier2')->first()->id;
        //Řidiči a tahače
        Driver::create([
            'name' => 'Jitka Rokosová',
            'tel' => '+420111222334',
            'carrier_id' => $id,
            'vehicle_id' => Vehicle::create([
                'rz' => '6S37864',
                'type_id' => '1',
                'height_id' => '1',
                'carrier_id' => $id,
            ])->id,
        ]);
        //Šasí
        Vehicle::create([
            'rz' => '5S36789',
            'type_id' => '2',
            'height_id' => '1',
            'carrier_id' => $id,
        ]);
        Vehicle::create([
            'rz' => '7S58734',
            'type_id' => '2',
            'height_id' => '2',
            'carrier_id' => $id,
        ]);
    }
}
