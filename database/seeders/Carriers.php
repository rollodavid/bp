<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Address;
use App\Models\Carrier;
use App\Models\CarrierContact;
use App\Models\Vehicle;

class Carriers extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $id = Carrier::create([
            'name' => 'Carrier1',
            'ico' => '12345678',
            'dic' => 'CZ12345678',
            'address_id' => Address::create([
                'city' => 'City1',
                'street' => 'Street1',
                'number' => '5',
                'psc' => '00011',
            ])->id,
        ])->id;
        CarrierContact::create([
            'carrier_id' => $id,
            'contactname' => 'Jan Vopršálek',
            'email' => 'test@test.cz',
            'tel' => '+420222444666',
        ]);

        $id = Carrier::create([
            'name' => 'Carrier2',
            'ico' => '87654321',
            'dic' => 'CZ87654321',
            'address_id' => Address::create([
                'city' => 'City2',
                'street' => 'Street2',
                'number' => '76/2',
                'psc' => '11100',
            ])->id,
        ])->id;
        CarrierContact::create([
            'carrier_id' => $id,
            'contactname' => 'Dušan Moucha',
            'email' => 'test2@test.cz',
            'tel' => '+420333555777',
        ]);
    }
}
