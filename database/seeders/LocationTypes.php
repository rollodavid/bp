<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\LocationType;

class LocationTypes extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        LocationType::create([
            'name' => 'Vyzvednutí',
        ]);
        LocationType::create([
            'name' => 'Clení',
        ]);
        LocationType::create([
            'name' => 'Vykládka',
        ]);
        LocationType::create([
            'name' => 'Nakládka',
        ]);
        LocationType::create([
            'name' => 'Vrácení',
        ]);
    }
}
