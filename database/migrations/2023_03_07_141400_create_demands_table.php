<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('demands', function (Blueprint $table) {
            $table->id();
            //Zákazník
            $table->string('name');
            $table->string('email');
            $table->foreignId('customer_id')->nullable()->constrained('customers')->onUpdate('cascade')->onDelete('cascade');
            //PJ
            $table->foreignId('type_id')->nullable()->constrained('types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('size_id')->nullable()->constrained('sizes')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('netto_pick');
            $table->integer('netto_load')->nullable();
            //Celní deklarace
            $table->string('customs_contactname')->nullable();
            $table->string('customs_email')->nullable();
            $table->string('customs_tel')->nullable();
            //Celní služby
            $table->boolean('service_new')->nullable();
            $table->boolean('service_end')->nullable();
            $table->boolean('service_free')->nullable();
            $table->integer('service_price')->nullable()->default(0);
            //Poznámka
            $table->text('note')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('demands');
    }
};