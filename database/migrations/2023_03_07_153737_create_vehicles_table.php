<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('rz');
            $table->foreignId('type_id')->nullable()->constrained('vehicle_types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('height_id')->nullable()->constrained('vehicle_heights')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('carrier_id')->nullable()->constrained('carriers')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vehicles');
    }
};
