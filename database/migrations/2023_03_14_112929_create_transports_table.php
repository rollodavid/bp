<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transports', function (Blueprint $table) {
            $table->id();
            //ILU
            $table->string('ilu')->nullable();
            //Zákazník
            $table->foreignId('customer_id')->nullable()->constrained('customers')->onUpdate('cascade')->onDelete('cascade');
            //Dopravce
            $table->foreignId('carrier_id')->nullable()->constrained('carriers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('driver_id')->nullable()->constrained('drivers')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('chassis_id')->nullable()->constrained('vehicles')->onUpdate('cascade')->onDelete('cascade');
            //PJ
            $table->foreignId('type_id')->nullable()->constrained('types')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('size_id')->nullable()->constrained('sizes')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('netto_pick');
            $table->integer('netto_load')->nullable();
            //Nacenění přepravy
            $table->integer('carrier_price')->nullable();
            $table->integer('free_hours')->nullable();
            $table->integer('waiting_price_by_hour')->nullable();
            $table->double('waiting_hours')->nullable();
            $table->integer('waiting')->nullable();
            $table->double('percentage')->nullable();
            $table->integer('carrier_total')->nullable();
            $table->integer('total')->nullable();
            //Celní deklarace
            $table->string('customs_contactname')->nullable();
            $table->string('customs_email')->nullable();
            $table->string('customs_tel')->nullable();
            //Celní služby
            $table->boolean('service_new')->nullable();
            $table->boolean('service_end')->nullable();
            $table->boolean('service_free')->nullable();
            $table->integer('service_price')->nullable();
            //Date
            $table->date('date')->nullable();
            //Note
            $table->text('travel_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transports');
    }
};
