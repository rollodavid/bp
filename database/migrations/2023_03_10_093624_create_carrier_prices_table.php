<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('carrier_prices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('demand_id')->nullable()->constrained('demands')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('carrier_id')->nullable()->constrained('carriers')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('carrier_price')->nullable();
            $table->integer('free')->nullable()->default(2);
            $table->integer('hour')->nullable()->default(20);
            $table->double('percentage')->nullable()->default(5);
            $table->integer('carrier_total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('carrier_prices');
    }
};
