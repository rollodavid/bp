<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\CarriersController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\DatabaseController;
use App\Http\Controllers\DemandsController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\EmailsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PriceListController;
use App\Http\Controllers\SizesController;
use App\Http\Controllers\TransportsController;
use App\Http\Controllers\TypesController;
use App\Http\Controllers\VehiclesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'login'    => true,
    'logout'   => true,
    'register' => false,
    'reset'    => true,
    'confirm'  => false, 
    'verify'   => false,
]);

Route::get('/home', [HomeController::class, 'index'])->name('home');

//'index','create','store','show','edit','update','destroy'
//->parameters(['prepravy' => 'transport']);

//Transports
Route::resource('transports', TransportsController::class)->except(['create','store','show']);
//Files
Route::get('/transports/files/{file}', [TransportsController::class, 'download']);
//Travel orders export
Route::get('/export/{transport}', [TransportsController::class, 'export']);

//PriceList
Route::resource('demands', PriceListController::class)->except(['create','store','show','destroy']);
//Demands
Route::resource('demands', DemandsController::class)->except(['index','edit','update','show','destroy']);
//Realize demand
Route::get('/realize/{demand}/{carrier}', [PriceListController::class, 'realize']);

//Database
Route::resource('database', DatabaseController::class)->except(['create','store','show','edit','update','destroy']);

//Customers
Route::resource('customers', CustomersController::class)->except('show');

//Carriers
Route::resource('carriers', CarriersController::class)->except('show');

//Drivers
Route::resource('drivers', DriverController::class)->except('index','show');

//Vehicles
Route::resource('vehicles', VehiclesController::class)->except('show');

//PJ types
Route::resource('types', TypesController::class)->except('index','show');

//Sizes
Route::resource('sizes', SizesController::class)->except('show');

#region Mail
//Zaslání poptávky dopravcům
Route::get('/demand-info/{demand}', [EmailsController::class, 'index_demand_info']);
Route::post('/demand-info', [EmailsController::class, 'send_demand_info']);
//Zaslání nacenění zákazníkovi
Route::get('/pricing/{demand}/{carrier_price}', [EmailsController::class, 'index_pricing']);
Route::post('/pricing', [EmailsController::class, 'send_pricing']);
#endregion
